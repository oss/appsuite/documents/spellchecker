/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#ifndef _BLOCKINGCOUNTER_HPP__
#define _BLOCKINGCOUNTER_HPP__

#include <queue>
#include <condition_variable>
#include <mutex>

#include "helper.hpp"

#define BLOCKINGCOUNTER_WAIT_INFINITE 0UL;

/*
 *
 */
template <typename T> class BlockingCounter {
public:

    /*
     *
     */
    BlockingCounter(const T maxCount = 1) :
        m_curCount(0),
        m_maxCount(std::max(maxCount, T(1))) {
    }

    /*
     *
     */
    virtual ~BlockingCounter() {
    }

    /*
     *
     */
    void increment() {
        std::unique_lock<std::mutex> lock(m_mutex);

        while (m_curCount >= m_maxCount) {
            m_incrementable.wait(lock);
        }

        ++m_curCount;
    }

    /*
     *
     */
    bool increment(ulong maxWaitTimeMillis) {
        const std::chrono::time_point<std::chrono::high_resolution_clock> waitUntilTimepoint = TimeHelper::getTimepoint(maxWaitTimeMillis);
        bool ret = true;

        {
            std::unique_lock<std::mutex> lock(m_mutex);

            while (ret && (m_curCount >= m_maxCount)) {
                ret = (std::cv_status::no_timeout == m_incrementable.wait_until(lock, waitUntilTimepoint));
            }

            if (ret) {
                ++m_curCount;
            }
        }

        return ret;
    }

    /*
     *
     */
    void decrement() {
        const std::unique_lock<std::mutex> lock(m_mutex);

        if (m_curCount > 0) {
            --m_curCount;
        }

        m_incrementable.notify_one();
    }

    /*
     *
     */
    T getMaximumCount() const {
        return m_maxCount;
    }

    /*
     *
     */
    T get() {
        const std::unique_lock<std::mutex> lock(m_mutex);
        return m_curCount;
    }

    /*
     *
     */
    void reset() {
        const std::unique_lock<std::mutex> lock(m_mutex);

        m_curCount = 0;
        m_incrementable.notify_all();
    }

private:

    std::mutex m_mutex;
    std::condition_variable m_incrementable;
    T m_curCount;
    T m_maxCount;

};

#endif //_BLOCKINGCOUNTER_HPP__