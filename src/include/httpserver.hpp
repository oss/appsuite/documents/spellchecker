/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#ifndef _HTTPSERVER_HPP__
#define _HTTPSERVER_HPP__

#define BOOST_NETWORK_ENABLE_HTTPS

#ifndef BOOST_ERROR_CODE_HEADER_ONLY
#define BOOST_ERROR_CODE_HEADER_ONLY
#endif // BOOST_ERROR_CODE_HEADER_ONLY

#ifndef BOOST_SYSTEM_NO_DEPRECATED
#define BOOST_SYSTEM_NO_DEPRECATED
#endif // BOOST_SYSTEM_NO_DEPRECATED

#include <boost/network/include/http/server.hpp>
#include <cstring>
#include <cstdio>
#include <iostream>
#include <json/json.h>

#include "blockingcounter.hpp"

/* HttpServer */
class HttpServer {

public:

    /*
     * HttpServer::HttpHandler
     */
    class HttpHandler {
    public:

        /*
         * HttpServer::HttpHandler::BodyReader
         */
        class BodyReader {
        public:

            /*
             * ReadResult
             */
            enum Result {
                OK = 0,
                ERROR_TIMEOUT = 1,
                ERROR_RATE_LIMIT = 2
            };

            // -----------------------------------------------------------------

            BodyReader(const ulong maxParallelBodyReads, const ulong requestTimeoutMillis) :
                m_bodyReadBlockingCounter(maxParallelBodyReads),
                m_requestTimeoutMillis(requestTimeoutMillis),
                m_requestRateLimitTimeoutMillis(std::min(250UL, std::max(1UL, (requestTimeoutMillis / std::max(1UL, maxParallelBodyReads))))) {
            }

            ~BodyReader() {
            }

            BodyReader::Result readBody(boost::network::http::server<HttpServer::HttpHandler>::connection_ptr connection, const boost::network::http::server<HttpServer::HttpHandler>::request& request, const std::string& requestName, const bool appendNewLine, std::string& body);

        private:

            BlockingCounter<ulong> m_bodyReadBlockingCounter;
            const ulong m_requestTimeoutMillis;
            const ulong m_requestRateLimitTimeoutMillis;
        };

    public:

        HttpHandler(const ulong maxParallelBodyReads = 1, const ulong requestTimeoutMillis = 15000);

        virtual ~HttpHandler();

        virtual bool handleRequest(boost::network::http::server<HttpHandler>::connection_ptr connection, const boost::network::http::server<HttpHandler>::request& request, const std::string& body) {

            return false;
        }

        virtual bool handleGetRequest(boost::network::http::server<HttpHandler>::connection_ptr connection, const boost::network::http::server<HttpHandler>::request& request, const std::string& body) {

            return false;
        }

        virtual bool handlePostRequest(boost::network::http::server<HttpHandler>::connection_ptr connection, const boost::network::http::server<HttpHandler>::request& request, const std::string& body) {

            return false;
        }

        virtual bool handlePutRequest(boost::network::http::server<HttpHandler>::connection_ptr connection, const boost::network::http::server<HttpHandler>::request& request, const std::string& body) {

            return false;
        }

        virtual bool handleDeleteRequest(boost::network::http::server<HttpHandler>::connection_ptr connection, const boost::network::http::server<HttpHandler>::request& request, const std::string& body) {

            return false;
        }

    public:

        void operator ()(const boost::network::http::server<HttpServer::HttpHandler>::request& request, boost::network::http::server<HttpServer::HttpHandler>::connection_ptr connection);

    protected:

        bool readJsonBody(const std::string& body, Json::Value& retValue);

        bool writeJsonResponse(boost::network::http::server<HttpHandler>::connection_ptr connection, const Json::Value& value);

        static void writeResponse(boost::network::http::server<HttpHandler>::connection_ptr connection, int status, const std::string& content, const std::string& contentType);

    private:

        BodyReader m_bodyReader;

        std::unique_ptr<Json::StreamWriterBuilder> m_streamWriterBuilder = std::make_unique<Json::StreamWriterBuilder>();

        std::unique_ptr<Json::CharReaderBuilder> m_stringReaderBuilder = std::make_unique<Json::CharReaderBuilder>();;

        const ulong m_requestTimeoutMillis;
    };

    /*
     *
     */
    HttpServer(const std::string& listenAdress, const ulong port, const ulong requestThreadCount, const ulong requestTimeoutMillis, HttpServer::HttpHandler& httpHandler, const std::string& sslCertFile, const std::string& sslKeyFile, const std::string& sslKeyPassword);

    /*
     *
     *
     */
    virtual ~HttpServer();

    virtual void run();

    virtual void stop();

protected:

    static std::string sslKeyPasswordCallback(std::size_t max_length, boost::asio::ssl::context_base::password_purpose purpose);

private:

    std::unique_ptr<boost::network::http::server<HttpServer::HttpHandler> > m_serverImpl;

    const ulong m_requestThreadCount;

    const ulong m_requestTimeoutMillis;
};

typedef HttpServer::HttpHandler HttpServerHandler;
typedef boost::network::http::server<HttpServerHandler> BoostHttpServer;

#endif // _HTTPSERVER_HPP__
