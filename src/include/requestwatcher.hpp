/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#ifndef _REQUESTWATCHER_HPP__
#define _REQUESTWATCHER_HPP__

#include <atomic>
#include <condition_variable>
#include <deque>
#include <functional>
#include <mutex>

#include "helper.hpp"

/*
 * RequestWatcher
 *
 * In order to start request watching and request timeout handling, a RequestWatcher instance
 * is created and the RequestWatcher#start method needs to be called.
 * Request watching can be stopped by calling RequestWatcher#stop.
 * When registering a request at the RequestWatcher at the  beginning of the request handler
 * to be watched for a timeout, a RequestWatcher::RequestEntry object is created and returned
 * to the caller.
 * When the request handling has finshed, the previously returned RequestWatcher::RequestEntry
 * object needs to be deregistered at the RequestWatcher.
 * The RequestWatcher::RequestEntry offers a method to update user data given as string data.
 * This user data may be used to log/process additional data of the request
 * in case the RequestWatcher internally detects this request to have a timeout.
 * The user data allows to reflect the current status/progress of a request in case
 * a timeout happens and the set timeout handler is called.
 * The called TimeoutHandler needs to handle the timeout situation appropriately.
 */
class RequestWatcher {
public:

    /*
     * RequestEntry
     *
     * Uses a combination of current timestamp in milliseconds
     * as well as an increasing, process based request number
     * to ensure unique identy even in case of a restarted process.
     * The string representation of a RequestEntry instance is calculated as:
     * "${TIMESTAMP_MILLIS}_${PROCESS_BASED_REQUEST_NUMBER}".
     */
    class RequestEntry {
    public:

        RequestEntry();

        // ---------------------------------------------------------------------

        RequestEntry(const std::string& requestName);

        // ---------------------------------------------------------------------

        virtual ~RequestEntry();

        // ---------------------------------------------------------------------

        bool operator ==(const RequestEntry& other) const {
            return (other.m_timestampMillis == m_timestampMillis) && (other.m_requestNumber == m_requestNumber);
        }

        // ---------------------------------------------------------------------

        void setTimeoutReached() {
            m_timeoutReached.store(true);
        }

        // ---------------------------------------------------------------------

        bool isTimeoutReached() const {
            return m_timeoutReached.load();
        }

        // ---------------------------------------------------------------------

        void disableTimeoutHandling() {
            const std::unique_lock<std::mutex> lock(m_mutex);

            m_timeoutHandlingEnabled = false;
            m_timeoutHandlingDisabledCondition.notify_one();
        }

        // ---------------------------------------------------------------------

        bool isTimeoutHandlingEnabled(const ulong maxWaitTimeMillis) {
            std::unique_lock<std::mutex> lock(m_mutex);

            if (m_timeoutHandlingEnabled) {
                m_timeoutHandlingDisabledCondition.wait_for(lock, std::chrono::milliseconds(maxWaitTimeMillis));
            }

            return m_timeoutHandlingEnabled;
        }

        // ---------------------------------------------------------------------

        std::string getId() const {
            return std::to_string(m_timestampMillis).append("_").append(std::to_string(m_requestNumber));
        }

        // ---------------------------------------------------------------------

        const ulong getTimestamp() const {
            return m_timestampMillis;
        }

        // ---------------------------------------------------------------------

        const std::string& getRequestName() const {
            return m_requestName;
        }

        // ---------------------------------------------------------------------

        std::string getRequestData() {
            const std::unique_lock<std::mutex> lock(m_mutex);
            return m_requestData;
        }

        // ---------------------------------------------------------------------

        const ulong getRequestDataTimestamp() {
            const std::unique_lock<std::mutex> lock(m_mutex);
            return m_requestDataTimestampMillis;
        }

        // ---------------------------------------------------------------------

        const ulong getCheckedWordCount() {
            const std::unique_lock<std::mutex> lock(m_mutex);
            return m_checkedWordCount;
        }

        // ---------------------------------------------------------------------

        void updateRequestData(const std::string& requestData, ulong checkedWordCount = 0) {
            const std::unique_lock<std::mutex> lock(m_mutex);
            m_requestData = requestData;
            m_requestDataTimestampMillis = TimeHelper::getCurrentTimestamp();

            if (checkedWordCount > m_checkedWordCount) {
                m_checkedWordCount = checkedWordCount;
            }
        }

    private:

        std::mutex m_mutex;
        ulong m_timestampMillis;
        ulong m_requestDataTimestampMillis;
        ulong m_requestNumber;
        ulong m_checkedWordCount;
        std::string m_requestName;
        std::string m_requestData;
        std::atomic_bool m_timeoutReached;
        std::condition_variable m_timeoutHandlingDisabledCondition;
        bool m_timeoutHandlingEnabled;
    };

    typedef std::shared_ptr<RequestWatcher::RequestEntry> RequestEntrySharedPtr;

public:

    RequestWatcher(const std::function<int(RequestEntrySharedPtr)> requestTimeoutFunction, const ulong timeoutMillis);

    // -------------------------------------------------------------------------

    virtual ~RequestWatcher();

    // -------------------------------------------------------------------------

    const RequestEntrySharedPtr registerRequest(const std::string& requestName);

    // -------------------------------------------------------------------------

    void unregisterRequest(const RequestEntrySharedPtr requestEntry);

private:

    RequestWatcher();

    // -------------------------------------------------------------------------

    void implValidateRequestEntries();

    // -------------------------------------------------------------------------

    void implStartValidationTimer(const ulong timeoutMillis);

    // -------------------------------------------------------------------------

    void implStopValidationTimer();

private:

    const std::function<int(RequestEntrySharedPtr)> m_requestTimeoutFunction;

    const ulong m_timeoutMillis;

    std::mutex m_mutex;

    std::condition_variable m_stopTimerCondition;

    std::deque<RequestEntrySharedPtr> m_deque;

    bool m_running = true;

    bool m_timerRunning = false;
};

typedef std::shared_ptr<RequestWatcher> RequestWatcherSharedPtr;
typedef RequestWatcher::RequestEntrySharedPtr RequestEntrySharedPtr;

#endif // _REQUESTWATCHER_HPP__