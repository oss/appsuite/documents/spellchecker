/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#ifndef _SPELLCHECK_HPP__
#define _SPELLCHECK_HPP__

#include <map>
#include <memory>
#include <mutex>
#include <ostream>
#include <string>
#include <hunspell/hunspell.hxx>
#include <json/json.h>

#include "dictmanager.hpp"
#include "requestwatcher.hpp"

/*
 * SpellCheck
 */
class SpellCheck {

public:

    /*
    * SpellCheck::Result
    */
    enum Result {
        OK = 0,
        ERROR_GENERAL = 1,
        ERROR_BAD_REQUEST = 2,
        ERROR_TIMEOUT = 3,
        ERROR_NO_DICTIONARY = 4
    };

public:

    /**
     * Ctor
     */
    SpellCheck(const std::shared_ptr<DictManager> dicionaryManager);

    // -------------------------------------------------------------------------

    /**
     * Dtor
     */
    virtual ~SpellCheck();

    /**
     *  returns the list of locales and encodings supported by the spell checker
     */
    const std::vector<LocaleAndEncoding>& getSupportedLocalesAndEncodings() const {
        return m_dictManager->getLocalesAndEncodings();
    }

    /**
     *  returns true if the list of locales and encodings hash one entry at least
     */
    bool hasSupportedLocalesAndEncodings() const {
        return m_dictManager->hasLocalesAndEncodings();
    }

    /**
     * @param requestEntry
     *  the current request status of the request that allows updating the current operation in progress
     * @param text
     *  a sequence of words, separated by dedicated separators
     * @param locale
     *  the locale to be used for spellchecking as given by #getSupportedLocalesAndEncodings
     * @param noReplacements
     *  set to true if replacements should not be retrieved
     * @param offset (in/out)
     *  contains an offset to be added to detected startPosition in result object; the offset is incremented with detected code points of text when function returns
     * @param retValue (out)
     *  JSONArray containing JSONObjects that contain a 'start' and and 'length' attribute of a spelling error
     */
    SpellCheck::Result checkSpelling(const RequestEntrySharedPtr requestEntry, const std::string& text, const std::string& locale, bool noReplacements, int& offset, Json::Value& retValue);

    /**
     * @param requestEntry
     *  the current request status of the request that allows updating the current operation in progress
     * @param portions
     *  array that contains the portions of the paragraph, with each portion being able to have a dedicated locale set.
     * @param noReplacements
     *  set to true if replacements should not be retrieved
     * @param retValue (out)
     *  JSONArray containing JSONObjects that contain a 'start' and 'length' attribute of a spelling error of the paragraph
     */
    SpellCheck::Result checkParagraphSpelling(const RequestEntrySharedPtr requestEntry, const Json::Value& portions, bool noReplacements, Json::Value& retValue);

    /**
     *
     * @param requestEntry
     *  the current request status of the request that allows updating the current operation in progress
     * @param word
     *  the word to retrieve suggestions for
     * @param locale
     *  the locale to be used for spellchecking as given by #getSupportedLocalesAndEncodings
     */
    SpellCheck::Result suggestReplacements(const RequestEntrySharedPtr requestEntry, const std::string& word, const std::string& locale, std::vector<std::string>& retReplacements);

    /**
     * @param requestEntry
     *  the current request status of the request that allows updating the current operation in progress
     * @param word
     *  word to be checked
     * @param locale
     *  the locale to be used for spellchecking as given by #getSupportedLocalesAndEncodings
     * @return true if word is detected as spelled wrong
     */
    SpellCheck::Result isMisspelled(const RequestEntrySharedPtr requestEntry, const std::string& word, const std::string& locale, bool& ret);

protected:

    std::vector<std::string> implSuggest(const RequestEntrySharedPtr requestEntry, const std::string& word, const std::shared_ptr<Hunspell> dictionary, const std::string& locale);

private:

    std::shared_ptr<DictManager> m_dictManager;
};

typedef std::shared_ptr<SpellCheck> SpellCheckSharedPtr;

#endif // _SPELLCHECK_HPP__
