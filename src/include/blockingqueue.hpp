/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#ifndef _BLOCKINGQUEUE_HPP__
#define _BLOCKINGQUEUE_HPP__

#include <queue>
#include <condition_variable>
#include <mutex>

/*
 *
 */
template <class T> class BlockingQueue {
public:

    /*
     *
     */
    BlockingQueue(int maxSize = 1) :
        m_curSize(0),
        m_maxSize(std::max(maxSize, 1)) {
    }

    /*
     *
     */
    virtual ~BlockingQueue() {
    }

    /*
     *
     */
    void add(T val) {
        std::unique_lock<std::mutex> lock(m_mutex);

        while (m_curSize >= m_maxSize) {
            m_notFull.wait(lock);
        }

        m_queue.push(val);
        ++m_curSize;

        m_notEmpty.notify_one();
    }

    /*
     *
     */
    T take() {
        std::unique_lock<std::mutex> lock(m_mutex);

        while (m_curSize <= 0) {
            m_notEmpty.wait(lock);
        }

        T ret = m_queue.front();
        m_queue.pop();
        --m_curSize;

        m_notFull.notify_one();

        return ret;
    }

    /*
     *
     */
    void clear() {
        std::unique_lock<std::mutex> lock(m_mutex);

        for (int i = 0; i < m_curSize; ++i) {
            m_queue.pop();
        }

        m_curSize = 0;

        m_notFull.notify_all();
    }

private:

    std::queue<T> m_queue;
    std::mutex m_mutex;
    std::condition_variable m_notEmpty;
    std::condition_variable m_notFull;
    int m_curSize;
    int m_maxSize;

};

#endif // _BLOCKINGQUEUE_HPP__