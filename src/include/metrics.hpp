/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#ifndef _METRICS_HPP__
#define _METRICS_HPP__

#include <iostream>
#include <memory>
#include <mutex>
#include <prometheus/counter.h>
#include <prometheus/exposer.h>
#include <prometheus/gauge.h>
#include <prometheus/histogram.h>
#include <prometheus/registry.h>
#include <unordered_map>

#include "spellcheck_config.hpp"
#include "dictmanager.hpp"
#include "logging.hpp"

#define LOCALES_TOTAL "total"

/*
 * Metrics
 */
class Metrics {

    typedef prometheus::Counter* CounterPtr;
    typedef prometheus::Gauge* GaugePtr;
    typedef prometheus::Histogram* HistogramPtr;

public:

    /*
     * DictionaryLoadStatus
     */
    enum DictionaryStatus {
        UNLOADED = 0x00000000,
        LOADED = 0x00000001,
        LOAD_ERROR = 0x00000002,
        ACCESSED = 0x00000003
    };

    /*
     * CacheCount
     */
    enum CacheCount {
        ACCESS = 0x00000000,
        HIT = 0x00000001,
        RESET = 0x00000002
    };

    /*
     * CacheStatus
     */
    enum CacheStatus {
        MAX = 0x00000000,
        CURRENT = 0x00000001
    };

public:

    static std::shared_ptr<Metrics> create(const std::string& listenAddress, const int serverStarts, const std::shared_ptr<SpellCheckConfig> config, const std::shared_ptr<DictManager> dictManager);

    // -------------------------------------------------------------------------

    static std::shared_ptr<Metrics> get();

    // -------------------------------------------------------------------------

    Metrics(const std::string& listenAddress, const int serverStarts, const std::shared_ptr<SpellCheckConfig> config, const std::shared_ptr<DictManager> dictManager);

    // -------------------------------------------------------------------------

    virtual ~Metrics() {
    }

public:

    static void incrementRequestCount(const std::string& requestName, int code) {
        const std::shared_ptr<Metrics> metrics = get();

        if (metrics && (metrics->m_requestCountsCode.find(requestName) != metrics->m_requestCountsCode.end())) {
            auto& requestCountCodeMap = metrics->m_requestCountsCode[requestName];

            if (requestCountCodeMap.find(code) != requestCountCodeMap.end()) {
                requestCountCodeMap[code]->Increment();
            }

            if (200 != code) {
                metrics->m_requestCountsError[requestName]->Increment();
            }

            metrics->m_requestCountTotal->Increment();
        }
    }

    // -------------------------------------------------------------------------

    static void updateRequestTime(const std::string& requestName, ulong requestTimeMillis) {
        const std::shared_ptr<Metrics> metrics = get();

        if (metrics) {
            const auto& requestTimesMap = metrics->m_requestTimes;
            const auto histogramIter = requestTimesMap.find(requestName);

            if (histogramIter != requestTimesMap.end()) {
                histogramIter->second->Observe(double(requestTimeMillis) * 0.001);
            }
        }
    }

    // -------------------------------------------------------------------------

    static void incrementDictionaryCount(const std::string& locale, const DictionaryStatus dictionaryStatus) {
        const std::shared_ptr<Metrics> metrics = get();

        if (metrics) {
            const auto& dictCounterToUse = metrics->m_dictionary[dictionaryStatus];
            const auto foundEntry = dictCounterToUse.find(locale);

            if (foundEntry != dictCounterToUse.end()) {
                foundEntry->second->Increment();
            }
        }
    }

    // -------------------------------------------------------------------------

    static void incrementSuggestionsCacheCount(const std::string& locale, bool hit, bool reset) {
        const std::shared_ptr<Metrics> metrics = get();

        if (metrics) {
            auto& suggestionsCacheCountAccess = metrics->m_suggestionsCache[CacheCount::ACCESS];

            suggestionsCacheCountAccess[locale]->Increment();
            suggestionsCacheCountAccess[LOCALES_TOTAL]->Increment();

            if (hit) {
                auto& suggestionsCacheCountHit = metrics->m_suggestionsCache[CacheCount::HIT];

                suggestionsCacheCountHit[locale]->Increment();
                suggestionsCacheCountHit[LOCALES_TOTAL]->Increment();
            }

            if (reset) {
                auto& suggestionsCacheCountReset = metrics->m_suggestionsCache[CacheCount::RESET];

                suggestionsCacheCountReset[locale]->Increment();
                suggestionsCacheCountReset[LOCALES_TOTAL]->Increment();
            }
        }
    }

    // -------------------------------------------------------------------------

    static void updateSuggestionsCacheCurrentCount(const std::string& locale, ulong count) {
        const std::shared_ptr<Metrics> metrics = get();

        if (metrics) {
            metrics->m_suggestionsCacheStatus[CacheStatus::CURRENT][locale]->Set(count);
        }
    }

private:

    prometheus::Exposer m_prometheusExposer;

    std::shared_ptr<prometheus::Registry> m_prometheusRegistry;

    prometheus::Family<prometheus::Gauge>& m_serverStatusFamily;

    prometheus::Family<prometheus::Counter>& m_requestCountFamily;

    prometheus::Family<prometheus::Histogram>& m_requestTimeFamily;

    prometheus::Family<prometheus::Counter>& m_dictionaryCountFamily;

    prometheus::Family<prometheus::Counter>& m_cacheCountFamily;

    prometheus::Family<prometheus::Gauge>& m_cacheStatusFamily;

    CounterPtr m_requestCountTotal;

    std::unordered_map<std::string, std::unordered_map<int, CounterPtr> > m_requestCountsCode;

    std::unordered_map<std::string, CounterPtr> m_requestCountsError;

    std::unordered_map<std::string, HistogramPtr> m_requestTimes;

    std::unordered_map<DictionaryStatus, std::unordered_map<std::string, CounterPtr> > m_dictionary;

    std::unordered_map<CacheCount, std::unordered_map<std::string, CounterPtr> > m_suggestionsCache;

    std::unordered_map<CacheStatus, std::unordered_map<std::string, GaugePtr> > m_suggestionsCacheStatus;

private:

    static std::mutex m_mutex;

    static std::shared_ptr<Metrics> m_singleton;
};

#endif // _METRICS_HPP__