/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#ifndef _SPELLCHECK_CONFIG_HPP__
#define _SPELLCHECK_CONFIG_HPP__

#include "config.hpp"

/*
 * SpellCheckConfig
 */
class SpellCheckConfig : public Config {
public:

    SpellCheckConfig(const std::string& configFile);

    // ------------------------------------------------------------------------

    virtual ~SpellCheckConfig() {
    }

    // ------------------------------------------------------------------------

    const std::string& getListenAddress() const {
        return m_listenAddress;
    }

    // ------------------------------------------------------------------------

    const std::string& getListenAddressMetrics() const {
        return m_listenAddressMetrics;
    }

    // ------------------------------------------------------------------------

    const ulong getPort() const {
        return m_port;
    }

    // ------------------------------------------------------------------------

    const ulong getPortMetrics() const {
        return m_portMetrics;
    }

    // ------------------------------------------------------------------------

    const bool isSSLEnabled() const {
        return m_sslEnable;
    }

    // ------------------------------------------------------------------------

    const std::string& getSSLCertFile() const {
        return m_sslCertFile;
    }

    // ------------------------------------------------------------------------

    const std::string& getSSLKeyFile() const {
        return m_sslKeyFile;
    }

    // ------------------------------------------------------------------------

    const std::string& getSSLKeyPassword() const {
        return m_sslKeyPassword;
    }

    // ------------------------------------------------------------------------

    const std::string& getDictionaryPath() const {
        return m_dictionaryPath;
    }

    // ------------------------------------------------------------------------

    const ulong getDictionaryTimeoutSeconds() const {
        return m_dictionaryTimeoutSeconds;
    }

    // ------------------------------------------------------------------------

    const ulong getSuggestionsMaxCacheSize() const {
        return m_suggestionsMaxCacheSize;
    }

    // ------------------------------------------------------------------------

    const ulong getThreadCount() const {
        return m_threadCount;
    }

    // ------------------------------------------------------------------------

    const ulong getRequestThreadCount() const {
        return m_requestThreadCount;
    }

    // ------------------------------------------------------------------------

    const ulong getRequestTimeout() const {
        return m_requestTimeoutMillis;
    }

    // ------------------------------------------------------------------------

    const Logger::SeverityLevel getLogSeverity() const {
        return m_logSeverity;
    }

    // ------------------------------------------------------------------------

    const std::string& getLogPath() const {
        return m_logPath;
    }

    // ------------------------------------------------------------------------

    const bool isLogConsole() const {
        return m_logConsole;
    }

private:

    std::string m_listenAddress;
    std::string m_listenAddressMetrics;

    ulong m_port;
    ulong m_portMetrics;

    bool m_sslEnable;
    std::string m_sslCertFile;
    std::string m_sslKeyFile;
    std::string m_sslKeyPassword;

    std::string m_dictionaryPath;
    ulong m_dictionaryTimeoutSeconds;
    ulong m_suggestionsMaxCacheSize;
    ulong m_threadCount;
    ulong m_requestThreadCount;
    ulong m_requestTimeoutMillis;

    Logger::SeverityLevel m_logSeverity;
    std::string m_logPath;
    bool m_logConsole;
};

typedef std::shared_ptr<SpellCheckConfig> SpellCheckConfigSharedPtr;

#endif // _CONFIG_HPP__
