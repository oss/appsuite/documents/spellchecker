/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#ifndef _SPELLCHECK_SERVICE__
#define _SPELLCHECK_SERVICE__

#include <cstring>
#include <cstdio>
#include <iostream>
#include <memory>
#include "blockingqueue.hpp"
#include "spellcheck_config.hpp"
#include "httpserver.hpp"
#include "metrics.hpp"
#include "requestwatcher.hpp"
#include "spellcheck.hpp"

typedef std::vector<SpellCheckSharedPtr> SpellCheckVector;
typedef BlockingQueue<SpellCheckSharedPtr> SpellCheckQueue;

/*
 *
 */
class SpellCheckService : public HttpServerHandler {

public:

    SpellCheckService(const SpellCheckVector& spellCheckVector, const SpellCheckConfigSharedPtr config);

    /* -------------------------------------------------------------------------
     * */

    virtual ~SpellCheckService();

    /* -------------------------------------------------------------------------
     * */

    virtual bool handleGetRequest(BoostHttpServer::connection_ptr connection, const BoostHttpServer::request& request, const std::string& body);

    /* -------------------------------------------------------------------------
     * */

    virtual bool handlePostRequest(BoostHttpServer::connection_ptr connection, const BoostHttpServer::request& request, const std::string& body);

private:

    /*
     * not to be used
     */
    SpellCheckService();

    /*
     * not to be used
     */
    SpellCheckService& operator =(const SpellCheckService&) {
        return *this;
    }

    void implHandleRequestResult(BoostHttpServer::connection_ptr connection, RequestEntrySharedPtr, const std::string & requestName, const SpellCheck::Result spellCheckResult, const Json::Value & jsonResponse);

private:

    SpellCheckConfigSharedPtr m_config;

    SpellCheckQueue m_spellCheckQueue;
};

#endif // _SPELLCHECK_SERVICE_HPP__
