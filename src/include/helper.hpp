/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#ifndef _HELPER_HPP__
#define _HELPER_HPP__

#include <algorithm>
#include <chrono>
#include <exception>
#include <functional>
#include <locale>
#include <limits>
#include <string>
#include <thread>

/*
 * CodeHelper
 */
class CodeHelper {
public:

    /*
     * Allows to wrap a block of code to be executed as a lambda function with a
     * lambda function called in case of an exception while executing the code block
     */
    static void catchAllBlock(const std::function<void(void)> codeBlockHandler, std::function<void(const std::string&)> exceptionHandler) {
        try {
            codeBlockHandler();
        } catch (const std::exception& e) {
            exceptionHandler(e.what());
        } catch (...) {
            const std::exception_ptr curException = std::current_exception();

            try {
                if (curException) {
                    std::rethrow_exception(curException);
                } else {
                    exceptionHandler("ExceptionUnknown");
                }
            } catch (const std::exception e) {
                exceptionHandler(e.what());
            }
        }
    }
};

/*
 * StringHelper
 */

#define UNICP_INVALID std::numeric_limits<wchar_t>::max()
#define UNICP_MAX_VALUE 0x10FFFF

class StringHelper {
public:

    /*
     * Searching for a start string
     */
    static bool startsWith(const std::string& str, const std::string& searchStr) {
        return (str.find(searchStr) == 0);
    }

    /*
     * Converts each character of the given string to lower case
     */
    static const std::string toLowerAscii(const std::string& str) {
        std::string ret = str;

        std::transform(ret.begin(), ret.end(), ret.begin(), [](u_char c) {
            return std::tolower(c);
        });

        return ret;
    }

    /*
     * Converts each character of the given string to upper case
     */
    static const std::string toUpperAscii(const std::string& str) {
        std::string ret = str;

        std::transform(ret.begin(), ret.end(), ret.begin(), [](u_char c) {
            return std::toupper(c);
        });

        return ret;
    }

    /*
     * remove single characters
     */
    static const std::string removeAscii(const std::string& str, const u_char searchChar) {
        std::string ret = str;
        std::remove_if(ret.begin(), ret.end(), [searchChar](char& c) {
            return searchChar == static_cast<u_char>(c);
        });

        return ret;
    }

    /*
     * remove two characters
     */
    static const std::string removeAsciis(const std::string& str, const u_char searchChar1, const u_char searchChar2) {
        std::string ret = str;
        std::remove_if(ret.begin(), ret.end(), [searchChar1, searchChar2](char& c) {
            return (searchChar1 == static_cast<u_char>(c)) || (searchChar2 == static_cast<u_char>(c));
        });

        return ret;
    }

    /*
     * Converts each character of the given string to upper case
     */
    static const std::string replaceAscii(const std::string& str, const u_char searchChar, const u_char replaceChar) {
        std::string ret = str;

        std::replace_if(ret.begin(), ret.end(), [searchChar](char& c) {
            return searchChar == static_cast<u_char>(c);
        }, replaceChar);

        return ret;
    }

    /*
     * removes following, duplicate characters (e.g. multiple spaces)
     */
    static const std::string removeDuplicateAscii(const std::string& str, const u_char searchChar) {
        std::string ret = str;
        std::string::iterator eraseIter = std::unique(ret.begin(), ret.end(), [searchChar](const u_char first, const u_char second) {
            return (first == searchChar) && (first == second);
        });

        ret.erase(eraseIter, ret.end());

        return ret;
    }

    /*
     * Converts 1-4 characters of the given UTF-8
     * character sequence to its Unicode codepoint.
     * Params:
     *  str: the UTF-8 character sequence
     *  maxChar: the maximum number of characters available in the sequence
     *  consumed: the number of characters used to extract the codepoint (out)
     *  ignoreInvalid: treat UTF8 codepoint sequences from 0xD800-0xDFFF as valid
     * Return:
     *  wchar_t: the valid codepoint extracted from the sequence or UNICP_INVALID in case of an error
     */
    static wchar_t toUniCP(const u_char* str, int maxChars, int& consumed, bool ignoreInvalid = true) {
        consumed = 0;

        if (str && (maxChars-- > 0)) {
            ++consumed;

            const wchar_t val1 = *str++;

            if (!(val1 & 0x80)) {
                // start byte: 00xxxxxx
                return val1;
            }

            if (maxChars--) {
                ++consumed;

                const wchar_t val2 = *str++;

                if ((val1 & 0xE0) == 0xC0) {
                    // start byte: 110xxxxx
                    return ((val1 & 0x1F) << 6) | (val2 & 0x3F);
                } else if (maxChars-- && (ignoreInvalid || (val1 != 0xED) || ((val2 & 0xA0) != 0xA0))) {
                    ++consumed;

                    const wchar_t val3 = *str++;

                    if ((val1 & 0xF0) == 0xE0) {
                        // start byte: 1110xxxx
                        return ((val1 & 0x0F) << 12) | ((val2 & 0x3F) << 6) | (val3 & 0x3F);
                    } else if (maxChars-- && ((val1 & 0xF8) == 0xF0)) {
                        // start byte: 11110xxx
                        ++consumed;

                        const wchar_t val4 = *str++;

                        return ((val1 & 0x0F) << 18) | ((val2 & 0x3F) << 12) | ((val3 & 0x3F) << 6) | (val4 & 0x3F);
                    }
                }
            }
        }

        return UNICP_INVALID;
    }

    /*
     * Converts the given string with characters in UTF8 encoding
     * into an encoded character sequence string in the range 0x00-0xFF.
     */
    static std::string toUniCP(const std::string& str) {
        const int strLen = str.length();

        if (strLen) {
            int leftStrLen = strLen;
            const u_char* strChars = (const u_char*) str.c_str();
            std::string uniCPStr(leftStrLen, 0);
            u_char* uniCPStrData = (u_char*) uniCPStr.data();
            int consumed = 0;
            int uniCPLen = 0;

            while (leftStrLen > 0) {
                // fill UniCP str with UTF8 decoded codepoints
                const wchar_t curUniCP = toUniCP(strChars, leftStrLen, consumed);

                if ((0 == curUniCP) || (0 == consumed)) {
                    break;
                }

                // update data and lenghts
                *uniCPStrData++ = curUniCP;
                ++uniCPLen;
                strChars += consumed;
                leftStrLen -= consumed;
            }

            uniCPStr.resize(uniCPLen);

            return uniCPStr;
        }

        return std::string();
    }

    /*
     * Converts a Unicode codepoint to UTF-8 characters
     * inserts the characters into the given character sequence
     * Params:
     *  codepoint: the Unicode codepoint to be converted
     *  str: the UTF-8 character sequence into which the UTF-8 character sequence is inserted
     *  maxChars: the maximum number of characters available to be filled in the sequence
     * Return:
     *  int: the number of inserted characters into the given character sequence.
     *       returns 0 for an invalid conversion.
     */
    static int toUTF8(const wchar_t codepoint, u_char* str, int maxChars, bool ignoreInvalid = true) {
        if (str && (maxChars > 0)) {
            if (codepoint <= 0x7F) {
                *str = codepoint;
                return 1;
            } else if ((maxChars > 1) && ((codepoint <= 0x7FF))) {
                *str++ = (codepoint >> 6) | 0xC0;
                *str = (codepoint & 0x3F) | 0x80;
                return 2;
            } else if (ignoreInvalid || (codepoint < 0xD800) || (codepoint > 0xDFFF)) {
                if ((maxChars > 2) && (codepoint <= 0xFFFF)) {
                    *str++ = (codepoint >> 12) | 0xE0;
                    *str++ = ((codepoint >> 6) & 0x3F) | 0x80;
                    *str = (codepoint & 0x3F) | 0x80;
                    return 3;
                } else if ((maxChars > 3) && (codepoint <= 0x10FFFF)) {
                    *str++ = (codepoint >> 18) | 0xF0;
                    *str++ = ((codepoint >> 12) & 0x3F) | 0x80;
                    *str++ = ((codepoint >> 6) & 0x3F) | 0x80;
                    *str = (codepoint & 0x3F) | 0x80;
                    return 4;
                }
            }
        }

        return 0;
    }

    /*
     * Converts the given string with characters in the range 0x00-0xFF
     * into an UTF8 encoded character sequence string.
     * For this type of UTF8 encoding, the maximum number of UTF8 sequence
     * characters is twice the number of characters in the given source string.
     */
    static std::string toUTF8(const std::string& str) {
        const int strLen = str.length();

        if (strLen) {
            int leftUtf8Len = (strLen << 1) + 1;
            const u_char* strChars = (const u_char*) str.c_str();
            std::string utf8Str(leftUtf8Len, 0);
            u_char* utf8StrData = (u_char*) utf8Str.data();
            int utf8Len = 0;

            for (int i = 0; i < strLen; ++i) {
                // fill new str with UTF8 encoded codepoints
                const int curCharCount = toUTF8(*strChars++, utf8StrData + utf8Len, leftUtf8Len);

                if (0 == curCharCount) {
                    break;
                }

                // update lenghts
                utf8Len += curCharCount;
                leftUtf8Len -= curCharCount;
            }

            utf8Str.resize(utf8Len);

            return utf8Str;
        }

        return std::string();
    }
};

/*
 * TimeHelper
 */

class TimeHelper {
public:

    /*
     * Return:
     *  The current time in millisecons since epoch
     */
    static ulong getCurrentTimestamp() {
        return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    }

    // -------------------------------------------------------------------------

    static ulong getTimeDistance(const ulong previousTimestamp) {
        return getCurrentTimestamp() - previousTimestamp;
    }

    // -------------------------------------------------------------------------

    static std::chrono::time_point<std::chrono::high_resolution_clock> getTimepoint(const long timeDistanceMillis = 0) {
        return std::chrono::time_point<std::chrono::high_resolution_clock>(std::chrono::system_clock::now() + std::chrono::milliseconds(timeDistanceMillis));
    }

    // -------------------------------------------------------------------------

    static void threadSleep(const ulong sleepTimeMillis) {
        std::this_thread::sleep_for(std::chrono::milliseconds(sleepTimeMillis));
    }
};

#endif // _HELPER_HPP__
