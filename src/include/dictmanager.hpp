/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#ifndef _DICTMANAGER_HPP__
#define _DICTMANAGER_HPP__

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstdio>

#include <map>
#include <unordered_map>
#include <memory>
#include <mutex>
#include <set>
#include <thread>

#include <hunspell/hunspell.hxx>

#include "helper.hpp"
#include "logging.hpp"

typedef std::pair<std::string, std::string> LocaleAndEncoding;
typedef std::unordered_map<std::string, std::vector<std::string> > SuggestionsCache;
typedef std::pair<SuggestionsCache, std::mutex> SuggestionsCacheAndMutex;

/*
 * DictManager
 */
class DictManager {

public:

    /*
     * DictEntry
     */
    class DictEntry {
    public:

        DictEntry(const std::string& affPath, const std::string& dicPath, const std::string& locale);

        // -------------------------------------------------------------------------

        virtual ~DictEntry();

        // -------------------------------------------------------------------------

        const std::string& getAffPath() const {
            return m_affPath;
        }

        // -------------------------------------------------------------------------

        const std::string& getDicPath() const {
            return m_dicPath;
        }

        // -------------------------------------------------------------------------

        const std::string& getLocale() const {
            return m_locale;
        }

        // -------------------------------------------------------------------------

        const std::string& getEncoding();

        // -------------------------------------------------------------------------

        std::shared_ptr<Hunspell> getDictionary();

        // -------------------------------------------------------------------------

        SuggestionsCacheAndMutex& getSuggestionsCacheAndMutex() {
            return m_suggestionsCacheAndMutex;
        }

        // -------------------------------------------------------------------------

        void unloadDictionaryIfTimeoutReached(ulong timeoutTimestampMillis);

    private:

        /*
         * !!! Method is not thread safe by itself => caller needs to synchronize before !!!
         */
        void implUnloadDictionary();

    private:

        std::recursive_mutex m_recMutex;

        std::string m_affPath;

        std::string m_dicPath;

        std::shared_ptr<Hunspell> m_dict;

        std::string m_locale;

        std::string m_encoding;

        SuggestionsCacheAndMutex m_suggestionsCacheAndMutex;

        ulong m_timestampMillis = 0;
    };

public:

    DictManager(const std::string& dictionaryPath, ulong dictTimeoutMillis, int suggestionsMaxCacheSize);

    // -------------------------------------------------------------------------

    virtual ~DictManager();

    // -------------------------------------------------------------------------

    bool isRunning() {
        return m_timeoutHandlerRunning.load();
    }

    // -------------------------------------------------------------------------

    bool isTerminated() {
        return !isRunning();
    }

    // -------------------------------------------------------------------------

    void startDictionaryTimeoutHandling();

    // -------------------------------------------------------------------------

    const std::vector<LocaleAndEncoding>& getLocalesAndEncodings() const {
        return m_dictLocalesAndEncodings;
    }

    // -------------------------------------------------------------------------

    bool hasLocalesAndEncodings() const {
        return (m_dictLocalesAndEncodings.size() > 0);
    }

    // -------------------------------------------------------------------------

    bool usesUTF8(const std::string& normalizeLocale);

    // -------------------------------------------------------------------------

    std::shared_ptr<Hunspell> getDictionary(const std::string& normalizedLocale);

    // -------------------------------------------------------------------------

    SuggestionsCacheAndMutex& getSuggestionsCacheAndMutex(const std::string& normalizedLocale) const;

    // ------------------------------------------------------------------------

    const int getSuggestionsMaxCacheSize() const {
        return m_suggestionsMaxCacheSize;
    }

public:

    static std::string getRealPath(const std::string& path);
    // -------------------------------------------------------------------------

    static std::string normalizeLocale(const std::string& locale);

private:

    std::vector<LocaleAndEncoding> implReadDictionaryLocales() const;

    // -------------------------------------------------------------------------

    void implInitLocaleToDictEntryMap();
    // -------------------------------------------------------------------------

    void implRunTimeoutHandler();

private:

    std::mutex m_statusChangedMutex;

    std::condition_variable m_statusChangedCondition;

    std::string m_dictPath;

    std::vector<LocaleAndEncoding> m_dictLocalesAndEncodings;

    std::map<std::string, std::shared_ptr<DictEntry> > m_localeToDictEntryMap;

    ulong m_dictTimeoutMillis;

    int m_suggestionsMaxCacheSize;

    std::thread m_dictTimeoutThread;

    std::atomic_bool m_timeoutHandlerRunning;
};

// ------------------------------------------------------------------------------

typedef std::shared_ptr<DictManager> DictManagerSharedPtr;

#endif // _DICTMANAGER_HPP__
