/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#ifndef _CONFIG_HPP__
#define _CONFIG_HPP__

#ifndef BOOST_ERROR_CODE_HEADER_ONLY
#define BOOST_ERROR_CODE_HEADER_ONLY
#endif // BOOST_ERROR_CODE_HEADER_ONLY

#ifndef BOOST_SYSTEM_NO_DEPRECATED
#define BOOST_SYSTEM_NO_DEPRECATED
#endif // BOOST_SYSTEM_NO_DEPRECATED

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <typeinfo>
#include <utility>
#include <vector>

#include "helper.hpp"
#include "logging.hpp"

// -----------------------------------------------------------------------------

#define CONFIG_STRING_EMPTY ""
#define CONFIG_STRING_DEFAULT "__DEF__"

/*
 * Config class
 */
class Config {
public:

    /*
     * ConfigEntry
     */
    struct ConfigEntry {
        std::string key;
        std::string defaultValue;
        std::string value = CONFIG_STRING_DEFAULT;
    };

    /*
     * ConfigEntryMap
     */
    typedef std::map<std::string, ConfigEntry> ConfigEntryMap;

public:

    Config(const std::string& configFile, const std::vector<ConfigEntry>& entries) {
        std::for_each(entries.cbegin(), entries.cend(), [&](const ConfigEntry& curEntry) -> void {
            m_entryMap[curEntry.key] = curEntry;
        });

        // override config values with values set in file
        implReadFromFile(configFile, entries);
    }

    // -------------------------------------------------------------------------

    const std::string getString(const std::string& key) const {
        return implGetStringValue(key);
    }

    // -------------------------------------------------------------------------

    const bool getBool(const std::string& key) const {
        return "true" == StringHelper::toLowerAscii(implGetStringValue(key));
    }

    // -------------------------------------------------------------------------

    const int getInt(const std::string& key) const {
        std::string value = implGetStringValue(key);

        try {
            return std::stoi(value);
        } catch (const std::exception& e) {
            std::cerr << "SpellCheck received exception while reading 'int' config value (key: " << key << ", value: " << value << "): " << e.what() << std::endl;
        }

        return 0;
    }

    // -------------------------------------------------------------------------

    const long getLong(const std::string& key) const {
        std::string value = implGetStringValue(key);

        try {
            return std::stol(value);
        } catch (const std::exception& e) {
            std::cerr << "SpellCheck received exception while reading 'long' config value (key: " << key << ", value: " << value << "): " << e.what() << std::endl;
        }

        return 0;
    }

    // -------------------------------------------------------------------------

    const ulong getULong(const std::string& key) const {
        std::string value = implGetStringValue(key);

        try {
            return std::stoul(value);
        } catch (const std::exception& e) {
            std::cerr << "SpellCheck received exception while reading 'long' config value (key: " << key << ", value: " << value << "): " << e.what() << std::endl;
        }

        return 0;
    }

    // -------------------------------------------------------------------------

    const double getDouble(const std::string& key) const {
        std::string value = implGetStringValue(key);

        try {
            return std::stod(value);
        } catch (const std::exception& e) {
            std::cerr << "SpellCheck received exception while reading 'double' config value (key: " << key << ", value: " << value << "): " << e.what() << std::endl;
        }

        return 0;
    }

    // ------------------------------------------------------------------------

    const bool isDebug() const {
        return m_debug;
    }

    // ------------------------------------------------------------------------

    void setDebug(const bool debug) {
        m_debug = debug;
    }

private:

    void implReadFromFile(const std::string& configFilePath, const std::vector<ConfigEntry>& entries) {
        try {
            std::ifstream configStm(configFilePath);

            if (configStm && configStm.is_open()) {
                std::string curLine;

                while (configStm.good()) {
                    std::getline(configStm, curLine);

                    boost::trim(curLine);

                    // if valid, uncommented entry is found, assign the given value or designated EMPTY value
                    if (!curLine.empty() && ('#' != curLine[0])) {
                        const std::size_t equalPos = curLine.find_first_of('=');
                        std::string key = (equalPos > 0) ? curLine.substr(0, equalPos) : curLine;
                        std::string value(curLine.substr(equalPos + 1));

                        boost::trim(key);
                        boost::trim(value);

                        if (std::find_if(entries.begin(), entries.end(), [&](const ConfigEntry& curEntry) -> bool {
                            return curEntry.key == key;
                        }) != entries.end()) {
                            // put entry to map and assign value if contained in predefined keys
                            m_entryMap[key].value = value;
                        } else {
                            // create and add ConfigEntry for yet unknown property
                            m_entryMap[key] = { key, CONFIG_STRING_EMPTY, value };
                        }
                    }
                }
            }
        } catch (const std::exception& e) {
            std::cerr << "SpellCheck received exception while reading configuration file " << configFilePath << ": "
            << e.what() << std::endl;
        }
    }

    // -------------------------------------------------------------------------

    const std::string implGetStringValue(const std::string& key) const {
        const char* envStr = getenv(implGetNormalizedEnvKey(key).c_str());

        if (nullptr != envStr) {
            return std::string(envStr);
        }

        const ConfigEntryMap::const_iterator iter = m_entryMap.find(key);

        if (iter != m_entryMap.end()) {
            const std::string readValue = iter->second.value;

            return (CONFIG_STRING_DEFAULT == readValue) ? iter->second.defaultValue : readValue;
        }

        return CONFIG_STRING_EMPTY;
    }

    // -------------------------------------------------------------------------

    std::string implGetNormalizedEnvKey(const std::string& key) const {
        return StringHelper::toUpperAscii(StringHelper::replaceAscii(StringHelper::replaceAscii(key, '.', '_'), '-', '_'));
    }

private:

    ConfigEntryMap m_entryMap;
    bool m_debug = false;
};

#endif // _CONFIG_HPP__