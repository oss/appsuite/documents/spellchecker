/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#ifndef _LOGGING_HPP__
#define _LOGGING_HPP__

#ifndef BOOST_ERROR_CODE_HEADER_ONLY
#define BOOST_ERROR_CODE_HEADER_ONLY
#endif // BOOST_ERROR_CODE_HEADER_ONLY

#ifndef BOOST_SYSTEM_NO_DEPRECATED
#define BOOST_SYSTEM_NO_DEPRECATED
#endif // BOOST_SYSTEM_NO_DEPRECATED

#include <memory>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions/formatter.hpp>
#include <json/json.h>

#define SPC_LOG Logger::get()

/*
 * Logger
 */
class Logger {

public:

    /*
     * Logger::SeverityLevel
     */
    typedef boost::log::v2s_mt_posix::trivial::severity_level SeverityLevel;

    /*
     * Logger::Config
     */
    class Config {
    public:

        Config() {
        }

        Config(const SeverityLevel& severityLevel, const std::string& path, const bool console) :
            m_severityLevel(severityLevel),
            m_path(path),
            m_console(console) {
        }

        ~Config() {
        }

    public:

        const SeverityLevel& getSeverityLevel() const {
            return m_severityLevel;
        }

        // ---------------------------------------------------------------------

        const std::string& getPath() const {
            return m_path;
        }

        // ---------------------------------------------------------------------

        const bool isConsole() const {
            return m_console;
        }

    private:

        const SeverityLevel m_severityLevel = SeverityLevel::info;
        const std::string m_path;
        const bool m_console = false;
    };

public:

    static std::shared_ptr<Logger> create(const Logger::Config& config);

    static std::shared_ptr<Logger> get();

public:

    // !!! not to be called directly, use Logger::create instead to properly initialize logging!! !
    Logger(const Logger::Config& config);

    virtual ~Logger();

public:

    void flush();

    // -------------------------------------------------------------------------

    bool isTrace() const {
        return m_config.getSeverityLevel() <= SeverityLevel::trace;
    }

    // -------------------------------------------------------------------------

    void trace(const std::string& message) const {
        log(SeverityLevel::trace, message);
    }

    // -------------------------------------------------------------------------

    void trace(const std::string& message, const Json::Value& jsonData) const {
        log(SeverityLevel::trace, message, &jsonData);
    }

    // -------------------------------------------------------------------------

    bool isDebug() const {
        return m_config.getSeverityLevel() <= SeverityLevel::debug;
    }

    // -------------------------------------------------------------------------

    void debug(const std::string& message) const {
        log(SeverityLevel::debug, message);
    }

    // -------------------------------------------------------------------------

    void debug(const std::string& message, const Json::Value& jsonData) const {
        log(SeverityLevel::debug, message, &jsonData);
    }

    // -------------------------------------------------------------------------

    bool isInfo() const {
        return m_config.getSeverityLevel() <= SeverityLevel::info;
    }

    // -------------------------------------------------------------------------

    void info(const std::string& message) const {
        log(SeverityLevel::info, message);
    }

    // -------------------------------------------------------------------------

    void info(const std::string& message, const Json::Value& jsonData) const {
        log(SeverityLevel::info, message, &jsonData);
    }

    // -------------------------------------------------------------------------

    bool isWarn() const {
        return m_config.getSeverityLevel() <= SeverityLevel::warning;
    }

    // -------------------------------------------------------------------------

    void warn(const std::string& message) const {
        log(SeverityLevel::warning, message);

    }

    // -------------------------------------------------------------------------

    void warn(const std::string& message, const Json::Value& jsonData) const {
        log(SeverityLevel::warning, message, &jsonData);
    }

    // -------------------------------------------------------------------------

    bool isError() const {
        return m_config.getSeverityLevel() <= SeverityLevel::error;
    }

    // -------------------------------------------------------------------------

    void error(const std::string& message) const {
        log(SeverityLevel::error, message);
    }

    // -------------------------------------------------------------------------

    void error(const std::string& message, const Json::Value& jsonData) const {
        log(SeverityLevel::error, message, &jsonData);
    }

    // -------------------------------------------------------------------------

    bool isFatal() const {
        return m_config.getSeverityLevel() <= SeverityLevel::fatal;
    }

    // -------------------------------------------------------------------------

    void fatal(const std::string& message) const {
        log(SeverityLevel::fatal, message);
    }

    // -------------------------------------------------------------------------

    void fatal(const std::string& message, const Json::Value& jsonData) const {
        log(SeverityLevel::fatal, message, &jsonData);
    }

protected:

    void log(const SeverityLevel level, const std::string& message, const Json::Value* pJsonData = nullptr) const;

private:

    Config m_config;

    std::unique_ptr<Json::StreamWriterBuilder> m_streamWriterBuilder = std::make_unique<Json::StreamWriterBuilder>();

    std::unique_ptr<Json::CharReaderBuilder> m_stringReaderBuilder = std::make_unique<Json::CharReaderBuilder>();
};

#endif // _LOGGING_HPP__
