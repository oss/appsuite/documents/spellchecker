/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#include "spellcheck_config.hpp"

/*
 * Defines
 */
#define KEY_LISTENADDRESS "com.openexchange.spellcheck.listenAddress"
#define KEY_LISTENADDRESS_METRICS "com.openexchange.spellcheck.listenAddress.metrics"
#define KEY_PORT "com.openexchange.spellcheck.port"
#define KEY_PORT_METRICS "com.openexchange.spellcheck.port.metrics"
#define KEY_SSL_ENABLE "com.openexchange.spellcheck.ssl.enable"
#define KEY_SSL_CERTFILE "com.openexchange.spellcheck.ssl.certFile"
#define KEY_SSL_KEYFILE "com.openexchange.spellcheck.ssl.keyFile"
#define KEY_SSL_KEYPASSWORD "com.openexchange.spellcheck.ssl.keyPassword"
#define KEY_DICTIONARYPATH "com.openexchange.spellcheck.dictionaryPath"
#define KEY_DICTIONARYTIMEOUT_SECONDS "com.openexchange.spellcheck.dictionaryTimeoutSeconds"
#define KEY_SUGGESTIONS_MAX_CACHE_SIZE "com.openexchange.spellcheck.suggestionsMaxCacheSize"
#define KEY_THREADCOUNT "com.openexchange.spellcheck.threadCount"
#define KEY_REQUESTTHREADCOUNT "com.openexchange.spellcheck.requestThreadCount"
#define KEY_REQUESTTIMEOUT_MILLIS "com.openexchange.spellcheck.requestTimeout"
#define KEY_LOG_SEVERITY "com.openexchange.spellcheck.log.level"
#define KEY_LOG_PATH "com.openexchange.spellcheck.log.path"
#define KEY_LOG_CONSOLE "com.openexchange.spellcheck.log.console"

/*
 * Namespaces
 */
using namespace std;

/*
 * Statics
 */
static const string STRING_SPELLCHECK_EMPTY;

// -----------------------------------------------------------------------------

static vector<Config::ConfigEntry> implCreateDefaultEntries() {
    vector<Config::ConfigEntry> ret;

    ret.push_back({ KEY_LISTENADDRESS, "0.0.0.0" });
    ret.push_back({ KEY_LISTENADDRESS_METRICS, "0.0.0.0" });

    ret.push_back({ KEY_PORT, "8003" });
    ret.push_back({ KEY_PORT_METRICS, "8002" });

    ret.push_back({ KEY_SSL_ENABLE, "false" });
    ret.push_back({ KEY_SSL_CERTFILE, STRING_SPELLCHECK_EMPTY });
    ret.push_back({ KEY_SSL_KEYFILE, STRING_SPELLCHECK_EMPTY });
    ret.push_back({ KEY_SSL_KEYPASSWORD, STRING_SPELLCHECK_EMPTY });

    ret.push_back({ KEY_DICTIONARYPATH, "/usr/share/hunspell" });
    ret.push_back({ KEY_DICTIONARYTIMEOUT_SECONDS, "600" });
    ret.push_back({ KEY_SUGGESTIONS_MAX_CACHE_SIZE, "1000" });

    ret.push_back({ KEY_THREADCOUNT, "4" });
    ret.push_back({ KEY_REQUESTTHREADCOUNT, "16" });
    ret.push_back({ KEY_REQUESTTIMEOUT_MILLIS, "14000" });

    ret.push_back({ KEY_LOG_SEVERITY, "info" });
    ret.push_back({ KEY_LOG_PATH, "/var/log/open-xchange/spellcheck" });
    ret.push_back({ KEY_LOG_CONSOLE, "false" });

    // inexpensive move semantics
    return ret;
}

/*
 * SpellCheckConfig
 */
SpellCheckConfig::SpellCheckConfig(const std::string& configFile) :
    Config(configFile, implCreateDefaultEntries()),
    m_listenAddress(getString(KEY_LISTENADDRESS)),
    m_listenAddressMetrics(getString(KEY_LISTENADDRESS_METRICS)),
    m_port(getULong(KEY_PORT)),
    m_portMetrics(getULong(KEY_PORT_METRICS)),
    m_sslEnable(getBool(KEY_SSL_ENABLE)),
    m_sslCertFile(getString(KEY_SSL_CERTFILE)),
    m_sslKeyFile(getString(KEY_SSL_KEYFILE)),
    m_sslKeyPassword(getString(KEY_SSL_KEYPASSWORD)),
    m_dictionaryPath(getString(KEY_DICTIONARYPATH)),
    m_dictionaryTimeoutSeconds(getULong(KEY_DICTIONARYTIMEOUT_SECONDS)),
    m_suggestionsMaxCacheSize(getULong(KEY_SUGGESTIONS_MAX_CACHE_SIZE)),
    m_threadCount(getULong(KEY_THREADCOUNT)),
    m_requestThreadCount(getULong(KEY_REQUESTTHREADCOUNT)),
    m_requestTimeoutMillis(getULong(KEY_REQUESTTIMEOUT_MILLIS)),
    m_logSeverity(Logger::SeverityLevel::info),
    m_logPath(getString(KEY_LOG_PATH)),
    m_logConsole(getBool(KEY_LOG_CONSOLE)) {

    // evaluate given logging level string and assign
    const string logSeverityStr = StringHelper::toLowerAscii(getString(KEY_LOG_SEVERITY));

    if (logSeverityStr == "trace") {
        m_logSeverity = Logger::SeverityLevel::trace;
    } else if (logSeverityStr == "debug") {
        m_logSeverity = Logger::SeverityLevel::debug;
    } else if (logSeverityStr == "info") {
        m_logSeverity = Logger::SeverityLevel::info;
    } else if (logSeverityStr == "warn") {
        m_logSeverity = Logger::SeverityLevel::warning;
    } else if (logSeverityStr == "error") {
        m_logSeverity = Logger::SeverityLevel::error;
    } else if (logSeverityStr == "fatal") {
        m_logSeverity = Logger::SeverityLevel::fatal;
    }

    // some warnings for misconfigured items
    if (m_requestThreadCount < 10) {
        SPC_LOG->warn(string("Spellcheck config item ") + KEY_REQUESTTHREADCOUNT + " is set to " + to_string(m_requestThreadCount) + ". Http(s) request handling might not work reliably. Recommended values are greater or equal to 10.");
    }
}
