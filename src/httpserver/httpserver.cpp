/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#include "httpserver.hpp"

#include <algorithm>
#include <atomic>
#include <boost/asio/buffer.hpp>
#include <boost/asio/ssl/context.hpp>
#include <boost/lexical_cast.hpp>
#include <condition_variable>
#include <mutex>
#include <string>

#include "helper.hpp"
#include "logging.hpp"
#include "metrics.hpp"

using namespace std;
using namespace boost::asio;
using namespace boost::placeholders;
using namespace Json;

static const string CONTENT_TYPE_JSON("application/json; charset=utf-8");

static const string REQUEST_GET("GET");
static const string REQUEST_POST("POST");
static const string REQUEST_PUT("PUT");
static const string REQUEST_DELETE("DELETE");

static const string STR_CLOSE("close");
static const string STR_CONTENT_TYPE("Content-Type");
static const string STR_CONTENT_LENGTH("Content-Length");
static const string STR_CONNECTION("Connection");

static string SSL_KEY_PASSWORD;

/*
 * BodyReaderImpl
 *
 * Internal helper class to read the body of a request in an asynchronous way internally.
 * The call to the BodyReaderImpl#readBody method itself is a synchronous call.
 * This class is to be instantianted and to be used only once for each body read.
 * As such, one instance of this class must not (!) be used for multiple body reads!
 */
class BodyReaderImpl : public boost::enable_shared_from_this<BodyReaderImpl> {
public:

    BodyReaderImpl(BoostHttpServer::request const& request, string& bodyToRead) :
        m_request(request),
        m_bodyToRead(bodyToRead) {
    }

    // -------------------------------------------------------------------------

    virtual ~BodyReaderImpl() {
        m_objectInvalid = true;
    }

    // -------------------------------------------------------------------------

    bool readBody(BoostHttpServer::connection_ptr connection, const ulong requestTimeoutMillis, const bool appendNewLine) {
        const BoostHttpServer::request::headers_container_type& headers = m_request.headers;
        int contentLength = 0;
        bool ret = false;

        m_bodyToRead.clear();
        m_bodyComplete = false;

        // try to retrieve content-length header first to determine size of body to read
        for (BoostHttpServer::request::headers_container_type::const_iterator it = headers.begin(); it != headers.end(); ++it) {
            if (StringHelper::toLowerAscii(it->name) == "content-length") {
                contentLength = boost::lexical_cast<int>(it->value);
                break;
            }
        }

        // read body only if content-length header is given and has a length > 0
        if (contentLength > 0) {
            // reserver space with a possible addition of a newline character
            m_bodyToRead.reserve(contentLength + 1);

            // recursively and asynchronously read
            // all internal chunks of the request body
            implReadChunks(contentLength, connection);

            {
                // wait for BODY_READ_TIMEOUT_TOTAL max. until all chunks have been asynchronously read
                unique_lock<mutex> lock(m_mutex);

                if (!m_bodyComplete) {
                    m_bodyCompleteCondition.wait_for(lock, std::chrono::milliseconds(requestTimeoutMillis));

                    if (!m_bodyComplete) {
                        m_objectInvalid = true;
                        return false;
                    }
                }
            }

            // append newline to valid body if requested
            if ((m_bodyToRead.length() > 0) && appendNewLine) {
                m_bodyToRead.push_back('\n');
            }

            ret = m_bodyComplete;
        } else {
            ret = true;
        }

        return ret;
    }

private:

    void implReadChunks(size_t leftBytes, BoostHttpServer::connection_ptr connection) {
        connection->read(boost::bind(&BodyReaderImpl::implHandlePostRead, BodyReaderImpl::shared_from_this(), _1, _2, _3, connection, leftBytes));
    }

    // -------------------------------------------------------------------------

    void implHandlePostRead(BoostHttpServer::connection::input_range range, boost::system::error_code error, size_t size, BoostHttpServer::connection_ptr connection, size_t leftBytes) {
        if (m_objectInvalid) {
            return;
        }

        try {
            bool finished = true;

            // finish reads in case of an error during chunk reads
            if (!error) {
                // append current chunk to body
                m_bodyToRead.append(boost::begin(range), size);

                // read additional chunks if necessary
                const size_t newLeftBytes = leftBytes - size;

                if (newLeftBytes > 0) {
                    finished = false;
                    implReadChunks(newLeftBytes, connection);
                }
            }

            if (finished) {
                // notify waiting thread, that body load has just finished
                unique_lock<mutex> lock(m_mutex);

                m_bodyComplete = true;
                m_bodyCompleteCondition.notify_one();
            }
        } catch (...) {
            SPC_LOG->trace("SpellCheck WebService handler caught exception in body read");
        }
    }

private:

    BoostHttpServer::request const& m_request;
    string& m_bodyToRead;

    mutex m_mutex;
    condition_variable m_bodyCompleteCondition;
    bool m_bodyComplete = false;
    bool m_objectInvalid = false;
};

/*
 * HttpServer::HttpHandler::BodyReader
 */
HttpServerHandler::BodyReader::Result HttpServerHandler::BodyReader::readBody(BoostHttpServer::connection_ptr connection, const boost::network::http::server<HttpServer::HttpHandler>::request& request, const string& requestName, const bool appendNewLine, string& body) {
    HttpServerHandler::BodyReader::Result bodyReadResult = HttpServerHandler::BodyReader::ERROR_TIMEOUT;

    // try to read body with a limited maximum count of parallel body reads by using the BlockingCounter
    // member, initialized with the maximum number of parallel body reads/request threads within Ctor;
    // wait for max. the BODY_READ_TIMEOUT_RATE_MILLIS period until the next, parallel body read is possible at all;
    // if body could not be read within this period of time, return false to indicate that reading the body was not successful
    if (m_bodyReadBlockingCounter.increment(m_requestRateLimitTimeoutMillis)) {
        CodeHelper::catchAllBlock([&]() {
            if (boost::make_shared<BodyReaderImpl>(request, body)->readBody(connection, m_requestTimeoutMillis, appendNewLine)) {
                bodyReadResult = HttpServerHandler::BodyReader::OK;
            }
        }, [&](const string& exceptionCause) {
            SPC_LOG->error(string("SpellCheck WebService handler received exception when reading request body; ") + requestName + "' request: " + exceptionCause);
        });

        m_bodyReadBlockingCounter.decrement();
    } else {
        bodyReadResult = HttpServerHandler::BodyReader::ERROR_RATE_LIMIT;
    }

    return bodyReadResult;
}

/*
 * HttpServerHandler
 */
HttpServerHandler::HttpHandler(const ulong requestThreadCount, const ulong requestTimeoutMillis) :
    m_bodyReader(requestThreadCount, requestTimeoutMillis), m_requestTimeoutMillis(requestTimeoutMillis) {

    (*m_streamWriterBuilder)["commentStyle"] = "None";
    (*m_streamWriterBuilder)["indentation"] = "";
    (*m_streamWriterBuilder)["enableYAMLCompatibility"] = false;
    (*m_streamWriterBuilder)["emitUTF8"] = true;
}

// -----------------------------------------------------------------------------

HttpServerHandler::~HttpHandler() {
}

// -----------------------------------------------------------------------------

void HttpServerHandler::operator ()(const BoostHttpServer::request& request, BoostHttpServer::connection_ptr connection) {
    const auto& requestMethod = request.method;
    const auto& requestDestination = request.destination;
    const auto requestName = ((requestDestination.length() > 0) && (requestDestination[0] == '/')) ? requestDestination.substr(1) : requestDestination;

    CodeHelper::catchAllBlock([&]() {
        string body;

        switch (m_bodyReader.readBody(connection, request, requestName, true, body)) {
        case (HttpServerHandler::BodyReader::OK): {
            bool handled = false;

            if (REQUEST_GET == requestMethod) {
                handled = handleGetRequest(connection, request, body);
            } else if (REQUEST_POST == requestMethod) {
                handled = handlePostRequest(connection, request, body);
            } else if (REQUEST_PUT == requestMethod) {
                handled = handlePutRequest(connection, request, body);
            } else if (REQUEST_DELETE == requestMethod) {
                handled = handleDeleteRequest(connection, request, body);
            }

            if (!handled && !handleRequest(connection, request, body)) {
                writeResponse(connection, BoostHttpServer::connection::bad_request, "Bad request!\n", "text/plain");
                Metrics::incrementRequestCount(requestName, 400);
            }

            break;
        }

        case (HttpServerHandler::BodyReader::ERROR_RATE_LIMIT): {
            SPC_LOG->error("SpellCheck not able to handle request due to request rate limit. Please consider incrementing the 'com.openexchange.spellcheck.requestThreadCount' configuration item as well as the maximum available virtual memory for service.");
            writeResponse(connection, 429, "Too many requests!\n", "text/plain");
            Metrics::incrementRequestCount(requestName, 429);
            break;
        }

        case (HttpServerHandler::BodyReader::ERROR_TIMEOUT):
        default: {
            SPC_LOG->error(string("SpellCheck not able to read request body within ") + to_string(m_requestTimeoutMillis) + "ms");
            writeResponse(connection, BoostHttpServer::connection::request_timeout, "Request timeout!\n", "text/plain");
            Metrics::incrementRequestCount(requestName, 408);
            break;
        }
        }
    }, [&](const string& exceptionCause) {
        SPC_LOG->error(string("SpellCheck received exception when handling Http(s) request: ") + exceptionCause);
        Metrics::incrementRequestCount(requestName, 500);
    });
}

// -----------------------------------------------------------------------------

bool HttpServerHandler::readJsonBody(const string& body, Value& retValue) {
    const unique_ptr<CharReader> reader(m_stringReaderBuilder->newCharReader());
    const char* pBody = body.c_str();
    Json::String errs;

    const bool ret = reader->parse(pBody, pBody + body.length(), &retValue, &errs);

    if (!ret) {
        SPC_LOG->error(string("SpellCheck error when parsing JSON body: ") + errs);
    }

    return ret;
}

// -----------------------------------------------------------------------------

bool HttpServerHandler::writeJsonResponse(BoostHttpServer::connection_ptr connection, const Value& value) {
    bool ret = true;

    try {
        HttpServerHandler::writeResponse(connection, BoostHttpServer::connection::ok, writeString(*m_streamWriterBuilder, value), CONTENT_TYPE_JSON);
    } catch (exception const& e) {
        SPC_LOG->error(string("SpellCheck received exception when writing JSON response: ") + e.what());
        ret = false;
    }

    return ret;
}

// -----------------------------------------------------------------------------

void HttpServerHandler::writeResponse(BoostHttpServer::connection_ptr connection, int status, const string& content, const string& contentType) {

    try {
        vector<BoostHttpServer::response_header> headers;

        headers.push_back({ STR_CONTENT_TYPE, contentType });
        headers.push_back({ STR_CONTENT_LENGTH, to_string(content.length()) });
        headers.push_back({ STR_CONNECTION, STR_CLOSE });

        connection->set_status(static_cast<BoostHttpServer::connection::status_t>(status));
        connection->set_headers(headers);
        connection->write(content);
    } catch (exception const& e) {
        SPC_LOG->error(string("SpellCheck received exception when writing status response: ") + e.what());
    }
}

// -----------------------------------------------------------------------------

HttpServer::HttpServer(const string& listenAdress, const ulong port, const ulong requestThreadCount, const ulong requestTimeoutMillis, HttpServerHandler& httpHandler, const string& sslCertFile, const string& sslKeyFile, const string& sslKeyPassword) :
    m_requestThreadCount(max(2UL, requestThreadCount)),
    m_requestTimeoutMillis(requestTimeoutMillis) {

    SSL_KEY_PASSWORD = sslKeyPassword;

    BoostHttpServer::options serverOptions(httpHandler);

    // set options; we need a thread pool with a minimum of 2 threads since CppNetLib is not able to asynchronously read bodies with thread count < 2
    serverOptions.address(listenAdress).port(to_string(port)).reuse_address(true).thread_pool(make_shared<boost::network::utils::thread_pool>(m_requestThreadCount));

    /* create SSL context if certificate and key file is given */
    if ((sslKeyFile.length() > 0) && (sslCertFile.length() > 0)) {
        shared_ptr<ssl::context> ctx = make_shared<ssl::context>(ssl::context::sslv23);

        ctx->set_options(ssl::context::default_workarounds);
        ctx->set_default_verify_paths();

        try {
            /* Set key and certifcate files as well as key password callback, if given*/
            if (sslKeyPassword.length() > 0) {
                ctx->set_password_callback(sslKeyPasswordCallback);
            }

            ctx->use_certificate_file(sslCertFile, ssl::context::pem);
            ctx->use_private_key_file(sslKeyFile, ssl::context::pem);

            SPC_LOG->info("SpellCheck WebService Https support is enabled");
        } catch (const exception& e) {
            SPC_LOG->error(string("SpellCheck received exception when creating SSL context. Starting WebService w/o Https support:") + e.what());
        }

        serverOptions.context(ctx);
    } else {
        SPC_LOG->warn("SpellCheck WebService Https support is disabled");
    }

    m_serverImpl.reset(new BoostHttpServer(serverOptions));
}

// -----------------------------------------------------------------------------

HttpServer::~HttpServer() {
}

// -----------------------------------------------------------------------------

void HttpServer::run() {
    if (m_serverImpl) {
        m_serverImpl->run();
    }
}

// -----------------------------------------------------------------------------

void HttpServer::stop() {
    if (m_serverImpl) {
        m_serverImpl->stop();
    }
}

// -----------------------------------------------------------------------------

string HttpServer::sslKeyPasswordCallback(size_t max_length, ssl::context_base::password_purpose purpose) {
    if (SSL_KEY_PASSWORD.length() > max_length) {
        SPC_LOG->error("Spellcheck is not able to use configured SSL key password due to length longer than " + to_string(max_length) + " characters!");
        return string();
    }

    return SSL_KEY_PASSWORD;
}
