/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#include <csignal>

#include "logging.hpp"
#include "spellcheck_service.hpp"

using namespace std;
using namespace Json;

/*
 * Contants
 */
static const string SPELLCHECK_SERVICE_NAME("Open-Xchange SpellCheck Service");
static const string SPELLCHECK_SERVICE_UUID("77fdc062-570b-11ea-91c3-8bd6e5445cb5");

static const unsigned int SPELLCHECK_API_NUMBER(1);
static const string SPELLCHECK_API_STRING(string("v").append(to_string(SPELLCHECK_API_NUMBER)));

static const string PATH_CHECK_SPELLING("check-spelling");
static const string PATH_CHECK_PARAGRAPH_SPELLING("check-paragraph-spelling");
static const string PATH_GET_SUPPORTED_LOCALES("supported-locales");
static const string PATH_HEALTH("health");
static const string PATH_LIVE("live");
static const string PATH_IS_MISSPELLED("is-misspelled");
static const string PATH_READY("ready");
static const string PATH_SUGGEST_REPLACEMENTS("suggest-replacements");

static const string KEY_ENCODING("encoding");
static const string KEY_HEALTH("health");
static const string KEY_LOCALE("locale");
static const string KEY_RESPONSE("response");
static const string KEY_SERVERAPITEXT("serverAPIText");
static const string KEY_SERVERAPIVERSION("serverAPI");
static const string KEY_SERVERID("serverId");
static const string KEY_SERVERNAME("serverName");
static const string KEY_SERVERSTATUS("serverStatus");
static const string KEY_STATUS("status");

static const string KEY_SPELL_RESULT("spellResult");
static const string KEY_SUPPORTED_LOCALES("supportedLocales");

static const string CONTENT_TYPE_TEXT("text/plain");

static const string CONNECTION_STATUS_BAD_REQUEST("Bad request");
static const string CONNECTION_STATUS_INTERNAL_SERVER_ERROR("Internal server error");
static const string CONNECTION_STATUS_NO_DICTIONARY("(No dictonary found for request locale)");
static const string CONNECTION_STATUS_REQUEST_TIMEOUT("Request timeout");

static const string STR_LIVE("LIVE");
static const string STR_NOT_READY("NOT READY");
static const string STR_NOT_RUNNNING_DICTIONARIES("NOT RUNNING DUE TO UNAVAILABLE DICTIONARIES");
static const string STR_READY("READY");
static const string STR_RUNNING("RUNNING");

static const string LOG_REQUEST_START("SpellCheck processed request: /");

static const ulong REQUEST_NOTHANDLED_TIMEOUT_MILLIS = 1000;

/*
 * RequestWatcherSingleton to be initalized in service Ctor
 */
static RequestWatcherSharedPtr requestWatcher;

/*
 * SpellCheckService
 */
SpellCheckService::SpellCheckService(const SpellCheckVector& spellCheckVector, const SpellCheckConfigSharedPtr config) :
    HttpServerHandler(max(1UL, config->getRequestThreadCount() - 1)),
    m_spellCheckQueue(spellCheckVector.size()),
    m_config(config) {

    for (auto curSpellCheck : spellCheckVector) {
        m_spellCheckQueue.add(curSpellCheck);
    }

    requestWatcher = make_shared<RequestWatcher>([this](RequestEntrySharedPtr requestEntry) {
        if (requestEntry) {
            SPC_LOG->error(string("SpellCheck WebService detected request timeout for request: ") + requestEntry->getRequestName() +
                           " (id: " + requestEntry->getId() +
                           ", duration: " + to_string(TimeHelper::getTimeDistance(requestEntry->getTimestamp())) + "ms" +
                           ", lastActionDuration: " + to_string(TimeHelper::getTimeDistance(requestEntry->getRequestDataTimestamp())) + "ms" +
                           ", checkedWordCount: " + to_string(requestEntry->getCheckedWordCount()) +
                           ", info: " + requestEntry->getRequestData() +
                           ")");

            // set TimeoutReached flag at requestEntry so that the working thread
            // is able to react on this and terminate itself appropriately
            requestEntry->setTimeoutReached();

            // check if request handler has already handled timeout for a
            // period of up to REQUEST_NOTHANDLED_TIMEOUT_MILLIS => return
            // SIGABRT in case the thread handler did not finally complete within
            // this termination waiting time period to notify caller that the
            // WebService needs to be restarted (e.g. in case Hunspell calls loop forever)
            if (requestEntry->isTimeoutHandlingEnabled(REQUEST_NOTHANDLED_TIMEOUT_MILLIS)) {
                SPC_LOG->info("SpellCheck detected not responding request handler => WebService is automatically restarted");
                return SIGABRT;
            }
        }

        // notify caller that the request has been handled completely (either
        // with valid or timeout response) and no WebService restart is needed
        return 0;
    }, config->getRequestTimeout());
}

/*
 *
 */
SpellCheckService::~SpellCheckService() {
}

/*
 *
 */
bool SpellCheckService::handleGetRequest(BoostHttpServer::connection_ptr connection, const BoostHttpServer::request& request, const string& body) {
    const string requestName = (request.destination.length() > 0) ? request.destination.substr(1) : "";

    if (requestName.empty()) {
        return false;
    }

    const SpellCheckSharedPtr spellCheck = m_spellCheckQueue.take();
    const RequestEntrySharedPtr requestEntry = requestWatcher->registerRequest(requestName);
    bool requestHandled = true;

    CodeHelper::catchAllBlock([&]() {
        if (PATH_LIVE == requestName) {
            HttpServerHandler::writeResponse(connection, BoostHttpServer::connection::ok, STR_LIVE, CONTENT_TYPE_TEXT);

            if (SPC_LOG->isTrace()) {
                Value jsonMessageData;

                jsonMessageData[KEY_RESPONSE] = STR_LIVE;

                SPC_LOG->trace(LOG_REQUEST_START + PATH_LIVE, jsonMessageData);
            }
        } else if (PATH_READY == requestName) {
            bool ready = true;

            if (spellCheck->hasSupportedLocalesAndEncodings()) {
                HttpServerHandler::writeResponse(connection, BoostHttpServer::connection::ok, STR_READY, CONTENT_TYPE_TEXT);
            } else {
                ready = false;
                HttpServerHandler::writeResponse(connection, BoostHttpServer::connection::service_unavailable, STR_NOT_READY, CONTENT_TYPE_TEXT);
            }

            if (SPC_LOG->isTrace()) {
                Value jsonMessageData;

                jsonMessageData[KEY_RESPONSE] = (ready ? STR_READY : STR_NOT_READY);

                SPC_LOG->trace(LOG_REQUEST_START + PATH_READY,  jsonMessageData);
            }
        }
        else if (PATH_HEALTH == requestName) {
            Value jsonStatus;
            Value jsonResponse;

            jsonStatus[KEY_SERVERNAME] = SPELLCHECK_SERVICE_NAME;
            jsonStatus[KEY_SERVERID] = SPELLCHECK_SERVICE_UUID;
            jsonStatus[KEY_SERVERAPIVERSION] = SPELLCHECK_API_NUMBER;
            jsonStatus[KEY_SERVERAPITEXT] = SPELLCHECK_API_STRING;
            jsonStatus[KEY_SERVERSTATUS] = spellCheck->hasSupportedLocalesAndEncodings() ? STR_RUNNING : STR_NOT_RUNNNING_DICTIONARIES;

            jsonResponse[KEY_HEALTH] = jsonStatus;

            implHandleRequestResult(connection, requestEntry, requestName, SpellCheck::OK, jsonResponse);
        } else if (PATH_GET_SUPPORTED_LOCALES == requestName) {
            const auto& localesAndEncodings = spellCheck->getSupportedLocalesAndEncodings();
            const auto count = localesAndEncodings.size();
            Value jsonLocalesAndEncodings(arrayValue);
            Value jsonResponse;

            jsonLocalesAndEncodings.resize(count);

            for (ArrayIndex i = 0; i < count; ++i) {
                const auto& curLocaleAndEncoding = localesAndEncodings[i];
                Value jsonLocaleAndEncoding;

                jsonLocaleAndEncoding[KEY_LOCALE] = curLocaleAndEncoding.first;
                jsonLocaleAndEncoding[KEY_ENCODING] = curLocaleAndEncoding.second;

                jsonLocalesAndEncodings[i] = jsonLocaleAndEncoding;
            }

            jsonResponse[KEY_SUPPORTED_LOCALES] = jsonLocalesAndEncodings;

            implHandleRequestResult(connection, requestEntry, requestName, SpellCheck::OK, jsonResponse);
        } else {
            requestHandled = false;
        }
    }, [&](const string& exceptionCause) {
        SPC_LOG->error(string("SpellCheck WebService GET handler received exception for '") + requestName + "' request: " + exceptionCause);
        requestHandled = false;
    });

    requestWatcher->unregisterRequest(requestEntry);
    m_spellCheckQueue.add(spellCheck);

    return requestHandled;
}

/*
 *
 */
bool SpellCheckService::handlePostRequest(BoostHttpServer::connection_ptr connection, const BoostHttpServer::request& request, const string& body) {
    const string requestName = (request.destination.length() > 0) ? request.destination.substr(1) : "";

    if (requestName.empty()) {
        return false;
    }

    const SpellCheckSharedPtr spellCheck = m_spellCheckQueue.take();
    const RequestEntrySharedPtr requestEntry = requestWatcher->registerRequest(requestName);
    Value jsonRequest;
    Value jsonResponse;
    const bool bodyRead = readJsonBody(body, jsonRequest);
    SpellCheck::Result spellCheckResult = SpellCheck::ERROR_BAD_REQUEST;

    CodeHelper::catchAllBlock([&]() {
        if (bodyRead && (PATH_CHECK_SPELLING == requestName)) {
            const Value& jsonNoReplacements = jsonRequest["noReplacements"];
            const bool noReplacements = jsonNoReplacements.isConvertibleTo(ValueType::booleanValue) ? jsonNoReplacements.asBool() : false;
            Value jsonSpellResult(arrayValue);
            int offset = jsonRequest["offset"].asInt();

            spellCheckResult = spellCheck->checkSpelling(requestEntry,
                                                         jsonRequest["text"].asString(),
                                                         jsonRequest["locale"].asString(),
                                                         noReplacements,
                                                         offset,
                                                         jsonSpellResult);

            jsonResponse[KEY_SPELL_RESULT] = jsonSpellResult;
        } else if (bodyRead && (PATH_CHECK_PARAGRAPH_SPELLING == requestName)) {
            const Value& jsonNoReplacements = jsonRequest["noReplacements"];
            const bool noReplacements = jsonNoReplacements.isConvertibleTo(ValueType::booleanValue) ? jsonNoReplacements.asBool() : false;
            Value jsonSpellResult(arrayValue);

            spellCheckResult = spellCheck->checkParagraphSpelling(requestEntry,
                                                                  jsonRequest["paragraph"],
                                                                  noReplacements,
                                                                  jsonSpellResult);

            jsonResponse[KEY_SPELL_RESULT] = jsonSpellResult;
        } else if (bodyRead && (PATH_SUGGEST_REPLACEMENTS == requestName)) {
            vector<string> replacements;
            Value jsonReplacements(arrayValue);

            spellCheckResult = spellCheck->suggestReplacements(requestEntry,
                                                               jsonRequest["word"].asString(),
                                                               jsonRequest["locale"].asString(),
                                                               replacements);

            const ArrayIndex count = replacements.size();

            jsonReplacements.resize(count);

            for (ArrayIndex i = 0; i < count; ++i) {
                jsonReplacements[i] = replacements[i];
            }

            jsonResponse[KEY_SPELL_RESULT] = jsonReplacements;
        } else if (bodyRead && (PATH_IS_MISSPELLED == requestName)) {
            bool isMisspelled = false;

            spellCheckResult = spellCheck->isMisspelled(requestEntry,
                                                        jsonRequest["word"].asString(),
                                                        jsonRequest["locale"].asString(),
                                                        isMisspelled);

            jsonResponse[KEY_SPELL_RESULT] = isMisspelled;
        }

        implHandleRequestResult(connection, requestEntry, requestName, spellCheckResult, jsonResponse);
    }, [&](const string& exceptionCause) {
        SPC_LOG->error(string("SpellCheck WebService POST handler received exception for '") + requestName + "' request: " + exceptionCause);

        // return an empty response in case of an exception
        jsonResponse[KEY_SPELL_RESULT] = Value(arrayValue);
        writeJsonResponse(connection, jsonResponse);
        spellCheckResult = SpellCheck::ERROR_GENERAL;
    });

    requestWatcher->unregisterRequest(requestEntry);
    m_spellCheckQueue.add(spellCheck);

    return true;
}

/*
 *
 */
void SpellCheckService::implHandleRequestResult(
    BoostHttpServer::connection_ptr connection,
    RequestEntrySharedPtr requestEntry,
    const string& requestName,
    const SpellCheck::Result spellCheckResult,
    const Value& jsonResponse) {

    BoostHttpServer::connection::status_t connectionStatus = BoostHttpServer::connection::ok;
    string connectionErrorStatusStr;
    string connectionErrorStatusAppendStr;
    const bool trace = SPC_LOG->isTrace();
    bool responded = false;

    connectionErrorStatusStr.reserve(128);

    switch (spellCheckResult) {
    case (SpellCheck::OK): {
        if (true == (responded = writeJsonResponse(connection, jsonResponse))) {
            if (trace) {
                Value jsonMessageData;

                jsonMessageData[KEY_RESPONSE] = jsonResponse;

                SPC_LOG->trace(LOG_REQUEST_START + requestName, jsonMessageData);
            }
        } else {
            connectionStatus = BoostHttpServer::connection::internal_server_error;
            connectionErrorStatusStr.append(CONNECTION_STATUS_INTERNAL_SERVER_ERROR);
        }

        break;
    }

    case (SpellCheck::ERROR_TIMEOUT): {
        connectionStatus = BoostHttpServer::connection::request_timeout;
        connectionErrorStatusStr.append(CONNECTION_STATUS_REQUEST_TIMEOUT);
        break;
    }

    case (SpellCheck::ERROR_NO_DICTIONARY): {
        connectionStatus = BoostHttpServer::connection::bad_request;
        connectionErrorStatusStr.append(CONNECTION_STATUS_BAD_REQUEST);
        connectionErrorStatusAppendStr = CONNECTION_STATUS_NO_DICTIONARY;
        break;
    }

    case (SpellCheck::ERROR_BAD_REQUEST): {
        connectionStatus = BoostHttpServer::connection::bad_request;
        connectionErrorStatusStr.append(CONNECTION_STATUS_BAD_REQUEST);
        break;
    }

    default: {
        connectionStatus = BoostHttpServer::connection::internal_server_error;
        connectionErrorStatusStr.append(CONNECTION_STATUS_INTERNAL_SERVER_ERROR);
        break;
    }
    }

    if (!responded) {
        connectionErrorStatusStr.append(": ").append(requestName);

        if (!connectionErrorStatusAppendStr.empty()) {
            connectionErrorStatusStr.push_back(' ');
            connectionErrorStatusStr.append(connectionErrorStatusAppendStr);
        }

        if (trace) {
            SPC_LOG->trace(string("SpellCheck WebService could not handle request: ") + connectionErrorStatusStr);
        }

        connectionErrorStatusStr.push_back('\n');
        writeResponse(connection, connectionStatus, connectionErrorStatusStr, CONTENT_TYPE_TEXT);
    }

    Metrics::incrementRequestCount(requestName, connectionStatus);
    Metrics::updateRequestTime(requestName, TimeHelper::getTimeDistance(requestEntry->getTimestamp()));
}
