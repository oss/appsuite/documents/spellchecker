/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#include "dictmanager.hpp"

#include <dirent.h>
#include <functional>
#include <iostream>

#include "helper.hpp"
#include "metrics.hpp"

using namespace std;

static const uint64_t CLEANUP_TIMEOUT_MILLIS = 1000;

/*
 * DictManager::DictEntry
 */
DictManager::DictEntry::DictEntry(const string& affPath, const string& dicPath, const string& locale) :
    m_affPath(affPath),
    m_dicPath(dicPath),
    m_locale(locale) {
}

// -------------------------------------------------------------------------

DictManager::DictEntry::~DictEntry() {
    lock_guard<recursive_mutex> guard(m_recMutex);
    implUnloadDictionary();
}

// -------------------------------------------------------------------------

const string& DictManager::DictEntry::getEncoding() {
    const lock_guard<recursive_mutex> guard(m_recMutex);
    string ret;

    if (m_encoding.empty()) {
        const ulong oldTimestampMillis = m_timestampMillis;

        // ::getDictionary initializes the m_encoding member
        getDictionary();

        // Unload dictionary if not loaded before
        if (0 == oldTimestampMillis) {
            implUnloadDictionary();
        }
    }

    return m_encoding;
}

// -------------------------------------------------------------------------

shared_ptr<Hunspell> DictManager::DictEntry::getDictionary() {
    const lock_guard<recursive_mutex> guard(m_recMutex);

    if (!m_dict) {
        try {
            m_dict = make_shared<Hunspell>(m_affPath.c_str(), m_dicPath.c_str());
            m_timestampMillis = TimeHelper::getCurrentTimestamp();
            m_encoding = m_dict->get_dict_encoding();

            Metrics::incrementDictionaryCount(m_locale, Metrics::DictionaryStatus::LOADED);

            if (SPC_LOG->isTrace()) {
                SPC_LOG->trace(string("SpellCheck loaded dictionary for locale: '") + getLocale() + '.' + m_encoding + "' (" + m_affPath + "; " + m_dicPath + ')');
            }
        } catch (const exception& e) {
            m_dict.reset();
            m_timestampMillis = 0;
            m_encoding.clear();

            Metrics::incrementDictionaryCount(m_locale, Metrics::DictionaryStatus::LOAD_ERROR);

            SPC_LOG->error(string("SpellCheck could not read dictionary for pathes (") + m_affPath + "; " + m_dicPath + "): " + e.what());
        }
    }

    return m_dict;
}

// -------------------------------------------------------------------------

void DictManager::DictEntry::unloadDictionaryIfTimeoutReached(ulong timeoutTimestampMillis) {
    const lock_guard<recursive_mutex> guard(m_recMutex);

    if ((m_timestampMillis > 0) && (m_timestampMillis <= timeoutTimestampMillis)) {
        implUnloadDictionary();
    }
}

/*
    * !!! Method is not thread safe by itself => caller needs to synchronize before !!!
    */
void DictManager::DictEntry::implUnloadDictionary() {
    if (m_dict) {
        m_dict.reset();
        m_timestampMillis = 0;

        Metrics::incrementDictionaryCount(m_locale, Metrics::DictionaryStatus::UNLOADED);

        if (SPC_LOG->isTrace()) {
            SPC_LOG->trace(string("SpellCheck unloaded dictionary for locale: '") + m_locale + '.' + m_encoding + "' (" + m_affPath + "; " + m_dicPath + ')');
        }
    }
}

/*
 * DictManager
 */
DictManager::DictManager(const string& dictionaryPath, uint64_t dictTimeoutMillis, int suggestionsMaxCacheSize) :
    m_dictPath(dictionaryPath),
    m_dictTimeoutMillis(dictTimeoutMillis),
    m_suggestionsMaxCacheSize(suggestionsMaxCacheSize),
    m_timeoutHandlerRunning(false) {

    const bool info = SPC_LOG->isInfo();

    if (info) {
        SPC_LOG->info(string("SpellCheck started initializing of dictionaries and encodings... (") + dictionaryPath + ')');
    }

    m_dictLocalesAndEncodings = implReadDictionaryLocales();
    implInitLocaleToDictEntryMap();

    if (info) {
        string localesStr;

        localesStr.reserve(1024);

        for (LocaleAndEncoding& curLocaleAndEncoding : m_dictLocalesAndEncodings) {

            if (localesStr.length() > 0) {
                localesStr += ", ";
            }

            ((localesStr += curLocaleAndEncoding.first) += '.') += curLocaleAndEncoding.second;
        }

        SPC_LOG->info(string("SpellCheck finished initializing of dictionaries and encodings: ") + localesStr);
    }
}

// -------------------------------------------------------------------------

DictManager::~DictManager() {
    bool expectedTrue = true;

    if (m_timeoutHandlerRunning.compare_exchange_strong(expectedTrue, false)) {
        m_statusChangedCondition.notify_one();
        m_dictTimeoutThread.join();
    }
}

// -------------------------------------------------------------------------

void DictManager::startDictionaryTimeoutHandling() {
    if (m_dictTimeoutMillis > 0) {
        m_timeoutHandlerRunning.store(true);
        m_dictTimeoutThread = thread(bind(&DictManager::implRunTimeoutHandler, this));

        SPC_LOG->info(string("SpellCheck started dictionary timeout handling with a timeout of ") + to_string(m_dictTimeoutMillis / 1000) + "s");
    }
}

// -------------------------------------------------------------------------

bool DictManager::usesUTF8(const string& normalizedLocale) {
    map<string, shared_ptr<DictEntry> >::iterator foundIter = m_localeToDictEntryMap.find(normalizedLocale);
    const string encoding = (foundIter != m_localeToDictEntryMap.end()) ?
                            StringHelper::toLowerAscii(foundIter->second->getEncoding()) :
                            "";

    return (encoding == "utf-8") || (encoding == "utf8");
}

// -------------------------------------------------------------------------

shared_ptr<Hunspell> DictManager::getDictionary(const string& normalizedLocale) {
    map<string, shared_ptr<DictEntry> >::iterator foundIter = m_localeToDictEntryMap.find(normalizedLocale);

    if (foundIter != m_localeToDictEntryMap.end()) {
        // increment dictionary access count
        Metrics::incrementDictionaryCount(normalizedLocale, Metrics::DictionaryStatus::ACCESSED);

        return foundIter->second->getDictionary();
    }

    return nullptr;
}

// -------------------------------------------------------------------------

SuggestionsCacheAndMutex& DictManager::getSuggestionsCacheAndMutex(const string& normalizedLocale) const {
    static SuggestionsCacheAndMutex dummy;

    const auto foundIter = m_localeToDictEntryMap.find(normalizedLocale);

    return (foundIter != m_localeToDictEntryMap.end()) ? foundIter->second->getSuggestionsCacheAndMutex() : dummy;
}

// -------------------------------------------------------------------------

string DictManager::getRealPath(const string& path) {
    string ret;

    try {
        char* targetPath = (path.length() > 0) ? realpath(path.c_str(), nullptr) : nullptr;

        if (targetPath) {
            ret = string(targetPath);
            free(targetPath);
        }
    } catch (const exception& e) {
        SPC_LOG->error(string("SpellCheck could not get real path for given path: ") + path);
    }

    return ret;
}

// -------------------------------------------------------------------------

string DictManager::normalizeLocale(const string& locale) {
    return StringHelper::replaceAscii(locale, '-', '_');
}

// -------------------------------------------------------------------------

vector<LocaleAndEncoding> DictManager::implReadDictionaryLocales() const {
    vector<LocaleAndEncoding> localesAndEncodings;
    DIR* pDir = NULL;

    try {
        pDir = (m_dictPath.empty() ? NULL : opendir(m_dictPath.c_str()));

        if (pDir) {
            for (dirent* pEntry = NULL; (NULL != (pEntry = readdir(pDir)));) {
                string name(pEntry->d_name);
                const int len = name.length();

                if ((len > 4) && (strcmp(pEntry->d_name + (len - 4), ".dic") == 0)) {
                    // just set the already known locale here since we don't know the encoding by now =>
                    // update of the encoding needed after creating the DictEntry!
                    localesAndEncodings.push_back(make_pair<string, string>(name.substr(0, (len - 4)), ""));
                }
            }
        } else {
            SPC_LOG->error(string("SpellCheck could not search dictionaryPath for locales: " + m_dictPath));
        }
    } catch (const exception& e) {
        SPC_LOG->error(string("SpellCheck could not read locales from given dictionaryPath: ") + m_dictPath + e.what());
    }

    if (pDir) {
        closedir(pDir);
    }

    return localesAndEncodings;
}

// -------------------------------------------------------------------------

void DictManager::implInitLocaleToDictEntryMap() {
    vector<shared_ptr<DictEntry> > foundDictEntries;

    for (LocaleAndEncoding& testLocaleAndEncoding : m_dictLocalesAndEncodings) {
        const string& testLocale = testLocaleAndEncoding.first;
        const string testAffPath = getRealPath(m_dictPath + '/' + testLocale + ".aff");
        const string testDicPath = getRealPath(m_dictPath + '/' + testLocale + ".dic");
        const vector<shared_ptr<DictEntry> >::iterator foundDictEntryIter =
            find_if(foundDictEntries.begin(), foundDictEntries.end(), [&testAffPath, &testDicPath](const shared_ptr<DictEntry> curDictEntry) {
            return (curDictEntry->getAffPath() == testAffPath) && (curDictEntry->getDicPath() == testDicPath);
        });

        shared_ptr<DictEntry> dictEntryToUse;

        // use exiting DictEntry or add new DictEntry, if not already
        // contained
        if (foundDictEntryIter != foundDictEntries.end()) {
            dictEntryToUse = *foundDictEntryIter;
        } else {
            foundDictEntries.push_back(dictEntryToUse = make_shared<DictEntry>(testAffPath, testDicPath, testLocale));
        }

        // use the found or created DictEntry for the LocalToDictEntry map
        m_localeToDictEntryMap[testLocale] = dictEntryToUse;

        // update encoding for the current LocaleAndEncoding pair
        testLocaleAndEncoding.second = dictEntryToUse->getEncoding();
    }
}

// -------------------------------------------------------------------------

void DictManager::implRunTimeoutHandler() {
    while (isRunning()) {
        {
            unique_lock<mutex> statusChangedLock(m_statusChangedMutex);
            m_statusChangedCondition.wait_for(statusChangedLock, chrono::milliseconds(CLEANUP_TIMEOUT_MILLIS), bind(&DictManager::isTerminated, this));
        }

        // determine the oldest timestamp;
        // loaded DictEntries with an earlier timestamp will be unloaded
        // we only need to cleanup as long as we're still running
        if (isRunning()) {
            const ulong timeoutTimestampMilis = TimeHelper::getTimeDistance(m_dictTimeoutMillis);

            for (map<string, shared_ptr<DictEntry> >::iterator mapIter = m_localeToDictEntryMap.begin(); mapIter != m_localeToDictEntryMap.end(); ++mapIter) {
                mapIter->second->unloadDictionaryIfTimeoutReached(timeoutTimestampMilis);
            }
        }
    }
}
