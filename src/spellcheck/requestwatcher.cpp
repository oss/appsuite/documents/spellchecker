/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#include <csignal>
#include <thread>
#include <vector>

#include "requestwatcher.hpp"

using namespace std;

static atomic_ulong requestCounter(0);
static constexpr ulong TIMEOUT_INVALID = numeric_limits<ulong>::max();

/*
 * RequestWatcher::RequestEntry
 */
RequestWatcher::RequestEntry::RequestEntry() :
    m_timestampMillis(TimeHelper::getCurrentTimestamp()),
    m_requestDataTimestampMillis(m_timestampMillis),
    m_requestNumber(++requestCounter),
    m_checkedWordCount(0),
    m_timeoutReached(false),
    m_timeoutHandlingEnabled(true) {
}

// -----------------------------------------------------------------------------

RequestWatcher::RequestEntry::RequestEntry(const std::string& requestName) :
    m_timestampMillis(TimeHelper::getCurrentTimestamp()),
    m_requestDataTimestampMillis(m_timestampMillis),
    m_requestNumber(++requestCounter),
    m_checkedWordCount(0),
    m_requestName(requestName),
    m_timeoutReached(false),
    m_timeoutHandlingEnabled(true) {
}

// -----------------------------------------------------------------------------

RequestWatcher::RequestEntry::~RequestEntry() {
}

/*
 * RequestWatcher
 */
RequestWatcher::RequestWatcher(const function<int(RequestEntrySharedPtr)> requestTimeoutFunction, ulong timeoutMillis) :
    m_requestTimeoutFunction(requestTimeoutFunction),
    m_timeoutMillis((0 == timeoutMillis) ? TIMEOUT_INVALID : timeoutMillis) {
}

// -----------------------------------------------------------------------------

RequestWatcher::~RequestWatcher() {
    implStopValidationTimer();
}

// -----------------------------------------------------------------------------

const RequestEntrySharedPtr RequestWatcher::registerRequest(const std::string& requestName) {
    RequestEntrySharedPtr ret;
    ulong nextTimeoutValue = TIMEOUT_INVALID;

    {
        // create and add new entry to deque to be returned
        const unique_lock<mutex> lock(m_mutex);

        m_deque.emplace_back(ret = make_shared<RequestWatcher::RequestEntry>(requestName));

        if (TIMEOUT_INVALID != m_timeoutMillis) {
            nextTimeoutValue = m_timeoutMillis - (TimeHelper::getCurrentTimestamp() - m_deque.front()->getTimestamp());
        }
    }

    // start timer with the calculated next timeout value outside of synchronized block;
    // if a timer is already running, it will get restarted automatically within the timeout validation call
    if (m_running && (TIMEOUT_INVALID != nextTimeoutValue)) {
        implStartValidationTimer(nextTimeoutValue);
    }

    return ret;
}

// -----------------------------------------------------------------------------

void RequestWatcher::unregisterRequest(const RequestEntrySharedPtr requestEntry) {
    if (requestEntry) {
        // disable timeout handling for just unregistered request
        // in every case as it does not need to be watched for timouts
        // anymore;
        // this also avoids the need for request handlers to call the
        // requestEntry#disableTimeoutHandling method by themselves in case
        // the handler finished its processing normally  and as such the
        // #unregister method needs to be called in any case
        requestEntry->disableTimeoutHandling();

        // remove entry from managed entries map
        const unique_lock<mutex> lock(m_mutex);

        m_deque.erase(std::remove_if(m_deque.begin(), m_deque.end(), [&](const RequestEntrySharedPtr& curEntry) {
            return *curEntry == *requestEntry;
        }), m_deque.end());
    }
}

// -----------------------------------------------------------------------------

void RequestWatcher::implValidateRequestEntries() {
    if (TIMEOUT_INVALID == m_timeoutMillis) {
        return;
    }

    vector<RequestEntrySharedPtr> timeoutEntries;
    ulong nextTimeoutMillis = TIMEOUT_INVALID;

    {
        // collect all timeout entries to call timeout handler outside of synchronized block
        const unique_lock<mutex> lock(m_mutex);
        const ulong currentTimestampMillis = TimeHelper::getCurrentTimestamp();
        const ulong invalidTimestampMillis = currentTimestampMillis - m_timeoutMillis;

        // if a timeout entry entry is found, remove from deque and add to timeout entry vector,
        // a possible second removal attempt by registerer is still allowed
        while (!m_deque.empty()) {
            const auto curEntry = m_deque.front();

            // check current entry for timeout
            if (curEntry->getTimestamp() <= invalidTimestampMillis) {
                timeoutEntries.emplace_back(curEntry);
                m_deque.pop_front();
            } else {
                // all entries are ordered by timestamp from front to back so
                // that we can leave the loop in case of the first valid entry
                break;
            }
        }

        if (m_running && !m_deque.empty()) {
            // determine next timer timeout value
            nextTimeoutMillis = m_timeoutMillis - (currentTimestampMillis - m_deque.front()->getTimestamp());
        }
    }

    // asynchronously call timeout handler for each found
    // timeout entry outside of the synchronized block
    for (const auto& curTimeoutEntry : timeoutEntries) {
        thread([this, curTimeoutEntry]() {
            const int signalCode = m_requestTimeoutFunction(curTimeoutEntry);

            // signal process if appropriate signal is returned from timeout handler
            if (signalCode > 0) {
                // instantly stop validation timer
                implStopValidationTimer();

                // signal process
                raise(signalCode);
            }
        }).detach();
    }

    // restart timer with new timeout if set to valid value
    if (TIMEOUT_INVALID != nextTimeoutMillis) {
        implStartValidationTimer(nextTimeoutMillis);
    }
}

// -----------------------------------------------------------------------------

void RequestWatcher::implStartValidationTimer(const ulong timeoutMillis) {
    const unique_lock<mutex> lock(m_mutex);

    if (m_running && !m_timerRunning) {
        m_timerRunning = true;

        // detach thread object, so that it will continue to run after leaving scope
        thread([this, timeoutMillis]() {
            {
                unique_lock<mutex> lock(m_mutex);

                // wait either for next timeout of if we got signaled that object is destroyed
                m_stopTimerCondition.wait_for(lock, chrono::milliseconds(timeoutMillis));
                m_timerRunning = false;

                // instantly return if no timer is needed anymore
                if (!m_running || m_deque.empty()) {
                    return;
                }
            }

            // validation call needs to be made outside of synchronized block
            implValidateRequestEntries();
        }).detach();
    }
}

// -----------------------------------------------------------------------------

void RequestWatcher::implStopValidationTimer() {
    const unique_lock<mutex> lock(m_mutex);

    // set flag so that validation timer should not do validation anymore
    m_running = false;

    // notify waiting timer condition to instantly stop timer
    m_stopTimerCondition.notify_all();
}