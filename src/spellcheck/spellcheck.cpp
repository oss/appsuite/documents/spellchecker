/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#include "spellcheck.hpp"

#include <cstring>
#include <dirent.h>

#include "dictmanager.hpp"
#include "helper.hpp"
#include "metrics.hpp"

/*
 * Defines
 */
#define CODEPOINT_MAP_SIZE 256

#define CODEPOINT_FLAG_SEP 0x00000001
#define CODEPOINT_FLAG_SEP_INCLUDED 0x00000002
#define CODEPOINT_FLAG_SEP_EOS 0x00000004

using namespace std;

typedef unsigned int UInt;

static const string REQINFO_LOCALES_START("Locales");
static const string REQINFO_LOCALES_END("Locales end");

static const string REQINFO_PARA_START("ParagraphSpelling");
static const string REQINFO_PARA_END("ParagraphSpelling end");

static const string REQINFO_TEXT_START("Text");
static const string REQINFO_TEXT_END("Text end");

static const string REQINFO_MISSPELLED_START("Misspelled");
static const string REQINFO_MISSPELLED_END("Misspelled end");

static const string REQINFO_WORD_START("Word");
static const string REQINFO_WORD_END("Word end");

static const string REQINFO_REPLACEMENTS_START("Replacements");
static const string REQINFO_REPLACEMENTS_END("Replacements end");

static const string REQINFO_SUGGEST("Suggestions");
static const string REQINFO_HUNSPELL_SUGGEST("Hunspell suggestions");
static const string REQINFO_HUNSPELL_SPELL("Hunspell spellcheck");
static const string REQINFO_RESPONSE("Response");

static const string LOCALE_INVALID;

static const vector<string> EMPTY_STRING_VECTOR;

/*
 * CPTester flag arrays
 */
static const wchar_t CODEPOINTS_SEP[] = {
    0, 32, 160, '\\', '\t', '\n', '\f', '\r', '.', ',', ';', '?', '!', '"', ':', '(', ')', '{', '}', '[', ']'
};

// -----------------------------------------------------------------------------

static const wchar_t CODEPOINTS_SEP_INCLUDED[] = {
    '.'
};

// -----------------------------------------------------------------------------

static const wchar_t CODEPOINTS_SEP_EOS[] = {
    0
};

/*
 * CPTester
 */
class CPTester {

public:

    CPTester() {
        memset(m_map, 0, CODEPOINT_MAP_SIZE * sizeof(UInt));

        for (int i = 0, count = sizeof(CODEPOINTS_SEP) / sizeof(wchar_t); i < count;) {
            m_map[CODEPOINTS_SEP[i++]] |= CODEPOINT_FLAG_SEP;
        }

        for (int i = 0, count = sizeof(CODEPOINTS_SEP_INCLUDED) / sizeof(wchar_t); i < count;) {
            m_map[CODEPOINTS_SEP_INCLUDED[i++]] |= CODEPOINT_FLAG_SEP_INCLUDED;
        }

        for (int i = 0, count = sizeof(CODEPOINTS_SEP_EOS) / sizeof(wchar_t); i < count;) {
            m_map[CODEPOINTS_SEP_EOS[i++]] |= CODEPOINT_FLAG_SEP_EOS;
        }
    }

    // -------------------------------------------------------------------------

    ~CPTester() {
        delete[] m_map;
    }

    // -------------------------------------------------------------------------

    inline bool isIncluded(const wchar_t codepoint) const {
        return !isSeparator(codepoint) ||
               (codepoint >= CODEPOINT_MAP_SIZE) ||
               ((m_map[codepoint] & CODEPOINT_FLAG_SEP_INCLUDED) == CODEPOINT_FLAG_SEP_INCLUDED);
    }

    // -------------------------------------------------------------------------

    inline bool isSeparator(const wchar_t codepoint) const {
        return (codepoint < CODEPOINT_MAP_SIZE) && ((m_map[codepoint] & CODEPOINT_FLAG_SEP) == CODEPOINT_FLAG_SEP);
    }

    // -------------------------------------------------------------------------

    inline bool isEOS(const wchar_t codepoint) const {
        return (codepoint < CODEPOINT_MAP_SIZE) && ((m_map[codepoint] & CODEPOINT_FLAG_SEP_EOS) == CODEPOINT_FLAG_SEP_EOS);
    }

private:

    UInt* m_map = new UInt[CODEPOINT_MAP_SIZE];
};

/*
 * CPTester singleton
 */
static const CPTester CP_TESTER;

/*
 * SpellCheck
 */
SpellCheck::SpellCheck(const shared_ptr<DictManager> dicionaryManager) :
    m_dictManager(dicionaryManager) {
}

// -----------------------------------------------------------------------------

SpellCheck::~SpellCheck() {
}

/*
 * All input and output strings of this method are encoded as UTF8 strings.
 * This is true even for strings that are finally language encoded in the range
 * 0x00-0xFF.
 * As a result, the language encoded characters in the range 0x00-0xFF are
 * encoded in UTF8 strings as well.
 * This finally affects the range of characters from 0x80-0xFF that need
 * 2 bytes in UTF8 encodings (e.g. 'Umlauts').
 * As a result, the iteration and separation of words from the given input string
 * works on an UTF8 codepoint basis.
 * For the final spellchecking step, the extracted UTF8 encoded words are decoded into
 * the appropriate 0x00-0xFF characters in case the used dictionary requires  language
 * encoded strings.
 * For UTF8 based language dictionaries, the appropriate UTF8 encoded character
 * sequences for an extracted word are used as given in the input string.
 * In case of spellcheck errors, all replacement suggestions need to be UTF8 encoded
 * as well. For UTF8 based dictionaries, the result can be used as is, for language
 * encoded dictionaries, the replacement suggestions need to be encoded to UTF8.
 */
SpellCheck::Result SpellCheck::checkSpelling(const RequestEntrySharedPtr requestEntry, const string& text, const string& locale, bool noReplacements, int& offset, Json::Value& retResult) {
    SpellCheck::Result ret = OK;

    const string normalizedLocale = DictManager::normalizeLocale(locale);
    const shared_ptr<Hunspell> dictionary = (text.length() > 0) ? m_dictManager->getDictionary(normalizedLocale) : nullptr;

    if (dictionary) {
        const string reqInfoLocale(string(" [") + normalizedLocale + "]");
        const string textToUse = text + " ";
        const u_char* charSeq = (const u_char*) text.c_str();
        const bool usesUTF8 = m_dictManager->usesUTF8(normalizedLocale);
        wchar_t curCP;
        basic_string<u_char> langEncTestWord;
        int checkedWordCount = 0;
        int leftCharLen = textToUse.length();
        int curCharPos = 0;
        int wordCharPos = 0;
        int curConsumedCharCount = 0;
        int wordCPPos = 0;
        int consumedCPCount = 0;

        requestEntry->updateRequestData(REQINFO_TEXT_START + reqInfoLocale);

        // Prereserve a char buffer with a capacity of max. 128 chars for each word (might grow automatically).
        // Since string treats '\0' characters as valid characters when string#push_back is called,
        // this string is used to count the number of word codepoints for the detected word in any case as well.
        // As such, it is neccesary to be filled even in UTF8 case with truncated 1 byte codepoints to finally
        // set the word codepoint count in response.
        // Summary: This string does not contain any valid characters in UTF8 case at all but is used to count codepoints.
        // For language encoded dictionaries, this string contains valid word characters in the range 0x00-0xFF.
        langEncTestWord.reserve(min(128, leftCharLen));

        while (!requestEntry->isTimeoutReached() && (UNICP_INVALID != (curCP = StringHelper::toUniCP(charSeq + curCharPos, leftCharLen, curConsumedCharCount)))) {
            // updated positions and left characters and consumed codepoints
            ++consumedCPCount;
            curCharPos += curConsumedCharCount;
            leftCharLen -= curConsumedCharCount;

            // store current codepoint for possible further word processing in language
            // encoding case as well as counter for word codepoints in every case
            langEncTestWord.push_back(curCP);

            // start spellchecking of single, detected word if separator has been found
            if (CP_TESTER.isSeparator(curCP)) {
                int endWordCharPos = curCharPos;

                requestEntry->updateRequestData(REQINFO_WORD_START + reqInfoLocale);

                // an e.g. dot ('.') is allowed for abbreviations and needs
                // to be added to the word although it is treated as a separator
                // => remove only separators that must not be taken into account
                if (!CP_TESTER.isIncluded(curCP)) {
                    endWordCharPos -= curConsumedCharCount;
                    langEncTestWord.pop_back();
                }

                // either use the original UTF8 encoded character sequences in case of UTF8 dictionaries
                // or use the language encoded codepoints (0x00-0xFF) for language encoded dictionaries
                const u_char* wordCharsBegin;
                const u_char* wordCharsEnd;

                if (usesUTF8) {
                    wordCharsBegin = (const u_char*) (charSeq + wordCharPos);
                    wordCharsEnd = (const u_char*) (charSeq + endWordCharPos);
                } else {
                    wordCharsBegin = langEncTestWord.data();
                    wordCharsEnd = wordCharsBegin + langEncTestWord.length();
                }

                const string testWord(wordCharsBegin, wordCharsEnd);
                const string spellInfo = string(" (") + testWord + reqInfoLocale + ")";

                /*  TODO: reenable
                    #65073# workaround for possible hunspell crash with composed unicode characters
                    we just replace high/low-surrogate characters with a replacement character.
                */

                requestEntry->updateRequestData(REQINFO_HUNSPELL_SPELL + spellInfo);

                // perform spellchecking of detected word and retrieve suggestions if misspelled
                if (!testWord.empty() && !dictionary->spell(testWord)) {
                    requestEntry->updateRequestData(REQINFO_SUGGEST + spellInfo);

                    const vector<string> replacementWordVector = noReplacements ? EMPTY_STRING_VECTOR : implSuggest(requestEntry, testWord, dictionary, normalizedLocale);

                    requestEntry->updateRequestData(REQINFO_RESPONSE + spellInfo);

                    Json::Value jsonCurResult(Json::objectValue);
                    Json::Value jsonReplacementArray(Json::arrayValue);

                    if (!replacementWordVector.empty()) {
                        jsonReplacementArray.resize(replacementWordVector.size());

                        vector<string>::const_iterator iter = replacementWordVector.begin();

                        for (int jsonArrayPos = 0; iter != replacementWordVector.end(); ++jsonArrayPos) {
                            const string& curReplacement = *iter++;
                            jsonReplacementArray[jsonArrayPos] = usesUTF8 ? curReplacement : StringHelper::toUTF8(curReplacement);
                        }
                    }

                    jsonCurResult["length"] = langEncTestWord.length();
                    jsonCurResult["locale"] = normalizedLocale;
                    jsonCurResult["start"] = wordCPPos + offset;
                    jsonCurResult["replacements"] = jsonReplacementArray;
                    jsonCurResult["word"] = usesUTF8 ? testWord : StringHelper::toUTF8(testWord);

                    retResult.append(jsonCurResult);
                }

                // word has been consumed => clear current char and codeoint words and update word position
                wordCharPos = curCharPos;
                wordCPPos = consumedCPCount;
                langEncTestWord.clear();

                requestEntry->updateRequestData(REQINFO_WORD_END + spellInfo, ++checkedWordCount);
            }
        }

        // update the given offset (in/out parameter) by the consumed code point count;
        // since the given text has been expanded by one space internally at the beginning
        // of the method, this last consumed code point must not be taken into account and
        // as such the given offset needs to be incremented by (consumedCPCount - 1);
        if (consumedCPCount > 0) {
            offset += (consumedCPCount - 1);
        }

        // set ERROR_TIMEOUT return value in case the thead has been
        // terminated by setting the timeout flag at requestEntry
        if (requestEntry->isTimeoutReached()) {
            ret = ERROR_TIMEOUT;
        }

        requestEntry->updateRequestData(REQINFO_TEXT_END + reqInfoLocale);
    } else {
        ret = ERROR_NO_DICTIONARY;
    }

    return ret;
}

// -----------------------------------------------------------------------------

SpellCheck::Result SpellCheck::checkParagraphSpelling(const RequestEntrySharedPtr requestEntry, const Json::Value& portions, bool noReplacements, Json::Value& retValue) {
    SpellCheck::Result ret = OK;

    requestEntry->updateRequestData(REQINFO_PARA_START);

    const UInt portionCount = portions.size();

    if (portionCount > 0) {
        string curLocale = DictManager::normalizeLocale(portions[0].get("locale", LOCALE_INVALID).asString());
        string nextLocale;
        string combinedText;
        int offset = 0;

        // walk over all portions, combine same, subsequent locale portions and check spelling for the combined text
        for (UInt curPos = 0; (OK == ret) && (curPos < portionCount); ++curPos) {
            combinedText.append(portions[curPos]["word"].asString());

            // check if this is the last portion or the next portion has a different locale
            if ((curPos == (portionCount - 1)) || (curLocale != (nextLocale = DictManager::normalizeLocale(portions[curPos + 1].get("locale", LOCALE_INVALID).asString())))) {
                if (!combinedText.empty()) {
                    ret = checkSpelling(requestEntry, combinedText, curLocale, noReplacements, offset, retValue);
                    combinedText.clear();
                }

                // assign next local as current locale
                curLocale = nextLocale;
            }
        }
    }

    requestEntry->updateRequestData(REQINFO_PARA_END);

    return ret;
}

// -----------------------------------------------------------------------------

SpellCheck::Result SpellCheck::suggestReplacements(const RequestEntrySharedPtr requestEntry, const string& word, const string& locale, vector<string>& retReplacements) {
    SpellCheck::Result ret = ERROR_TIMEOUT;

    const string normalizedLocale = DictManager::normalizeLocale(locale);
    const shared_ptr<Hunspell> dictionary(m_dictManager->getDictionary(normalizedLocale));
    const string spellInfo = string(" (") + word + " [" + normalizedLocale + "])";

    requestEntry->updateRequestData(REQINFO_REPLACEMENTS_START + spellInfo);
    retReplacements.clear();

    if (dictionary) {
        if (!word.empty()) {
            const bool usesUTF8 = m_dictManager->usesUTF8(normalizedLocale);

            requestEntry->updateRequestData(REQINFO_SUGGEST + spellInfo);
            retReplacements = implSuggest(requestEntry, usesUTF8 ? word : StringHelper::toUniCP(word), dictionary, normalizedLocale);

            if (!usesUTF8) {
                for (auto& curReplacement : retReplacements) {
                    curReplacement = StringHelper::toUTF8(curReplacement);
                }
            }
        }

        ret = OK;
    } else {
        ret = ERROR_NO_DICTIONARY;
    }

    requestEntry->updateRequestData(REQINFO_REPLACEMENTS_END + spellInfo, 1);

    return ret;
}

// -----------------------------------------------------------------------------

SpellCheck::Result SpellCheck::isMisspelled(const RequestEntrySharedPtr requestEntry, const string& word, const string& locale, bool& retMisspelled) {
    SpellCheck::Result ret = ERROR_TIMEOUT;

    const string normalizedLocale = DictManager::normalizeLocale(locale);
    const shared_ptr<Hunspell> dictionary(m_dictManager->getDictionary(normalizedLocale));

    if (dictionary) {
        const string spellInfo = string(" (") + word + " [" + normalizedLocale + "])";

        requestEntry->updateRequestData(REQINFO_MISSPELLED_START + spellInfo);
        retMisspelled = !dictionary->spell(m_dictManager->usesUTF8(normalizedLocale) ? word : StringHelper::toUniCP(word));
        requestEntry->updateRequestData(REQINFO_MISSPELLED_END + spellInfo, 1);

        ret = OK;
    } else {
        retMisspelled = false;
        ret = ERROR_NO_DICTIONARY;
    }

    return ret;
}

/*
 * Preconditions:
 *  - dictionary is valid
 *  - word is not empty
 *  - locale is normalized and matches the dictionary locale
 */
vector<string> SpellCheck::implSuggest(const RequestEntrySharedPtr requestEntry, const string& word, const shared_ptr<Hunspell> dictionary, const string& locale) {
    const int suggestionsMaxCacheSize = m_dictManager->getSuggestionsMaxCacheSize();
    const string reqInfoHunspellSuggest = REQINFO_HUNSPELL_SUGGEST + " (" + word + " [" + locale + "])";

    if (suggestionsMaxCacheSize > 0) {
        // We must be sure that the DictEntry exist.
        // For that reason, getDictionary() was called above, even if not needed in case of a cache hit
        auto& suggestionsCacheAndMutex = m_dictManager->getSuggestionsCacheAndMutex(locale);
        auto& suggestionsCache = suggestionsCacheAndMutex.first;
        auto& suggestionsCacheMutex = suggestionsCacheAndMutex.second;

        {
            // Try to find a cached element first and return this if found.
            // Cache access needs to be secured by a lock due to multithreaded access
            const unique_lock<mutex> suggestionsCacheLock(suggestionsCacheMutex);
            const auto entry = suggestionsCache.find(word);

            if (entry != suggestionsCache.end()) {
                // increment cache access count without reset and with hit
                Metrics::incrementSuggestionsCacheCount(locale, true, false);
                return entry->second;
            }
        }

        // Retrieve suggestions vector and add to cache
        requestEntry->updateRequestData(reqInfoHunspellSuggest);

        const vector<string> replacements = dictionary->suggest(word);

        // lock locale related cache after(!) suggestion retrieval for performance reasons
        const unique_lock<mutex> suggestionsCacheLock(suggestionsCacheMutex);
        const bool cacheReset = suggestionsCache.size() >= suggestionsMaxCacheSize;

        // dead simple - and we avoid a lot of overhead on every access and on shrink
        // so this is the most efficient implementation if cache limit is high enough
        if (cacheReset) {
            suggestionsCache.clear();
        }

        suggestionsCache.emplace(word, replacements);

        // increment cache access count with reset flag and without hit
        Metrics::incrementSuggestionsCacheCount(locale, false, cacheReset);

        // Update current cache fill count
        Metrics::updateSuggestionsCacheCurrentCount(locale, suggestionsCache.size());

        return replacements;
    }

    requestEntry->updateRequestData(reqInfoHunspellSuggest, 1);

    return dictionary->suggest(word);
}
