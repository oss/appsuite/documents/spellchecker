/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#include "logging.hpp"
#include "helper.hpp"

#include <string>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <boost/core/null_deleter.hpp>
#include <boost/date_time.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/async_frontend.hpp>
#include <boost/log/sinks/basic_sink_backend.hpp>
#include <boost/log/sinks/sink.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <mutex>
#include <sstream>

#define SPC_LOG_FILENAME "spellcheck.log"

namespace logging = boost::log;
namespace attr = logging::attributes;
namespace aux = logging::aux;
namespace expr = logging::expressions;
namespace sinks = logging::sinks;
namespace sources = logging::sources;

using namespace boost::gregorian;
using namespace boost::local_time;
using namespace boost::posix_time;
using namespace Json;
using namespace std;

static const string SEVERITY_NAME_TRACE = "TRACE";
static const string SEVERITY_NAME_DEBUG = "DEBUG";
static const string SEVERITY_NAME_INFO = "INFO";
static const string SEVERITY_NAME_WARNING = "WARNING";
static const string SEVERITY_NAME_ERROR = "ERROR";
static const string SEVERITY_NAME_CRITICAL = "CRITICAL";

static const string JSON_KEY_DATA = "data";
static const string JSON_KEY_JSON_MSG = "dataMsg";
static const string JSON_KEY_DETAILS = "details";
static const string JSON_KEY_LEVEL = "level";
static const string JSON_KEY_LEVEL_NAME = "levelName";
static const string JSON_KEY_MSG = "msg";
static const string JSON_KEY_MSG_DATA = "msgData";
static const string JSON_KEY_RECORD_NUMBER = "recordNumber";
static const string JSON_KEY_TIMESTAMP = "timestamp";
static const string JSON_KEY_TIMESTAMP_READABLE = "timestampReadable";

static const string JSON_DATA_MSG_START = "{\"" + JSON_KEY_JSON_MSG + "\":";
static const string JSON_DATA_MSG_END = "}";

static const size_t JSON_MESSAGE_LEN_SUBTRACTOR = JSON_DATA_MSG_START.length() - 1;

static const ptime UTC_EPOCH_TIME(date(1970, 1, 1));

typedef sinks::asynchronous_sink<sinks::text_ostream_backend> TextLogSink;

typedef pair<const string*, int> GrayLogLevel;

/**
 * @brief
 */
static weak_ptr<Logger> LOG;

/**
 * @brief Define attribute keywords for registered attributes
 */
BOOST_LOG_ATTRIBUTE_KEYWORD(log_recordNumber, "LineID", unsigned int)
BOOST_LOG_ATTRIBUTE_KEYWORD(log_severity, "Severity", Logger::SeverityLevel)
BOOST_LOG_ATTRIBUTE_KEYWORD(log_timestamp, "TimeStamp", boost::posix_time::ptime)

/**
 * @brief
 *
 */
class JSONSinkBackend : public sinks::basic_sink_backend<sinks::combine_requirements<sinks::concurrent_feeding, sinks::flushing>::type> {
public:
    /**
     * @brief Construct a new JSONSinkBackend object
     *
     * @param ostm
     */
    explicit JSONSinkBackend(ostream& oStm, StreamWriterBuilder& streamWriterBuilder, CharReaderBuilder& stringReaderBuilder) :
        m_oStm(oStm),
        m_streamWriterBuilder(streamWriterBuilder),
        m_stringReaderBuilder(stringReaderBuilder) {

        m_severityToGrayLogLevelMap[Logger::SeverityLevel::trace] = make_pair<const string*, int>(&SEVERITY_NAME_TRACE, 8);
        m_severityToGrayLogLevelMap[Logger::SeverityLevel::debug] = make_pair<const string*, int>(&SEVERITY_NAME_DEBUG, 7);
        m_severityToGrayLogLevelMap[Logger::SeverityLevel::info] = make_pair<const string*, int>(&SEVERITY_NAME_INFO, 6);
        m_severityToGrayLogLevelMap[Logger::SeverityLevel::warning] = make_pair<const string*, int>(&SEVERITY_NAME_WARNING, 4);
        m_severityToGrayLogLevelMap[Logger::SeverityLevel::error] = make_pair<const string*, int>(&SEVERITY_NAME_ERROR, 3);
        m_severityToGrayLogLevelMap[Logger::SeverityLevel::fatal] = make_pair<const string*, int>(&SEVERITY_NAME_CRITICAL, 2);
    }

public:

    /**
     * @brief
     *
     * @param rec
     */
    void consume(logging::record_view const& rec) {
        const string& msg = rec[expr::smessage].get();
        const GrayLogLevel& grayLogLevel = m_severityToGrayLogLevelMap[static_cast<Logger::SeverityLevel>(rec[log_severity].get())];
        const ptime& utcTime = rec[log_timestamp].get();
        Value jsonValue;
        Value jsonDetailsValue;

        // mandatory JSON based log output
        jsonValue[JSON_KEY_LEVEL] = grayLogLevel.second;
        jsonValue[JSON_KEY_TIMESTAMP] = (long) (utcTime - UTC_EPOCH_TIME).total_milliseconds();

        // optional JSON details
        jsonDetailsValue[JSON_KEY_RECORD_NUMBER] = rec[log_recordNumber].get();
        jsonDetailsValue[JSON_KEY_LEVEL_NAME] = *grayLogLevel.first;

        // set readable timestamp at details
        string utcISOStr = to_iso_extended_string(utcTime);
        const size_t utcISOStrLen = utcISOStr.length();

        if (utcISOStrLen > 3) {
            utcISOStr.resize(utcISOStrLen - 3);
        }

        jsonDetailsValue[JSON_KEY_TIMESTAMP_READABLE] = (utcISOStr + "Z");

        if (StringHelper::startsWith(msg, JSON_DATA_MSG_START)) {
            const string jsonMessageStr = msg.substr(JSON_DATA_MSG_START.length(), msg.length() - JSON_MESSAGE_LEN_SUBTRACTOR);
            const unique_ptr<CharReader> reader(m_stringReaderBuilder.newCharReader());
            const char* pJsonMessageStr = jsonMessageStr.c_str();
            Value jsonDataMsgContent;
            Json::String errs;

            if (reader->parse(pJsonMessageStr, pJsonMessageStr + jsonMessageStr.length(), &jsonDataMsgContent, &errs)) {
                jsonValue[JSON_KEY_MSG] = jsonDataMsgContent[JSON_KEY_MSG];
                jsonDetailsValue[JSON_KEY_MSG_DATA] = jsonDataMsgContent[JSON_KEY_DATA];
            } else {
                jsonValue[JSON_KEY_MSG] = msg;
                SPC_LOG->warn(string("SpellCheck error when parsing JSON body for log output: ") + errs);
            }
        } else {
            jsonValue[JSON_KEY_MSG] = msg;
        }

        // set JSON details object at JSON value object
        jsonValue[JSON_KEY_DETAILS] = jsonDetailsValue;

        m_oStm << StringHelper::removeAsciis(Json::writeString(m_streamWriterBuilder, jsonValue), '\r', '\n') << endl;
    }

    /**
     * @brief
     *
     */
    void flush() {
        m_oStm.flush();
    }

private:

    ostream& m_oStm;

    StreamWriterBuilder& m_streamWriterBuilder;

    CharReaderBuilder& m_stringReaderBuilder;

    map<Logger::SeverityLevel, GrayLogLevel> m_severityToGrayLogLevelMap;
};

/*
 * Logger
 */
Logger::Logger(const Logger::Config& config) :
    m_config(config) {

    (*m_streamWriterBuilder)["commentStyle"] = "None";
    (*m_streamWriterBuilder)["indentation"] = "";
    (*m_streamWriterBuilder)["enableYAMLCompatibility"] = false;
    (*m_streamWriterBuilder)["emitUTF8"] = true;
    (*m_streamWriterBuilder)["precisionType"] = "decimal";
    (*m_streamWriterBuilder)["precision"] = 3;

    const boost::shared_ptr<logging::core> logCore = logging::core::get();
    const string& logPath = config.getPath();

    logCore->set_filter(log_severity >= config.getSeverityLevel());

    if (!logPath.empty()) {
        try {
            const boost::shared_ptr<sinks::text_ostream_backend> fileBackend = boost::make_shared<sinks::text_ostream_backend>();

            fileBackend->auto_flush(true);

            // add file stream to file sink, set filter and formatter
            const logging::formatter fileFormatter = expr::stream << expr::attr<boost::posix_time::ptime>("TimeStamp") << " (" << log_severity << ")\t" << expr::smessage;
            boost::shared_ptr<TextLogSink> fileSink = boost::make_shared<TextLogSink>(fileBackend);
            const string logFilename = logPath + '/' + SPC_LOG_FILENAME;
            boost::shared_ptr<ofstream> logFileStm = boost::make_shared<ofstream>(logFilename, ofstream::out | ofstream::app);

            if (logFileStm->is_open() && (*logFileStm << endl).flush().good()) {
                fileSink->locked_backend()->add_stream(logFileStm);
                fileSink->set_formatter(fileFormatter);

                logCore->add_sink(fileSink);
            } else {
                cerr << "SpellCheck could not create/open configured log file to write to: " << logPath << endl;
            }
        } catch (const exception& e) {
            cerr << "SpellCheck received Exception when initializing file logging to " << logPath << ": " << e.what() << endl;
        }
    }

    // Console log
    if (config.isConsole()) {
        try {
            logCore->add_sink(boost::make_shared<sinks::asynchronous_sink<JSONSinkBackend>>(boost::make_shared<JSONSinkBackend>(cout, *m_streamWriterBuilder, *m_stringReaderBuilder)));
        } catch (const exception& e) {
            cerr << "SpellCheck received Exception when initializing file logging to " << logPath << ": " << e.what() << endl;
        }
    }

    logCore->add_global_attribute(aux::default_attribute_names::line_id(), attr::counter<unsigned int>(1));
    logCore->add_global_attribute(aux::default_attribute_names::timestamp(), attr::utc_clock());
}

/*
 *
 */
Logger::~Logger() {
    logging::core::get()->remove_all_sinks();
}

/*
 *
 */
shared_ptr<Logger> Logger::create(const Config& logConfig) {
    shared_ptr<Logger> ret;

    if (LOG.expired()) {
        LOG = ret = make_shared<Logger>(logConfig);
    } else {
        ret = LOG.lock();
    }

    return ret;
}
/*
 *
 */
shared_ptr<Logger> Logger::get() {
    return LOG.lock();
}

/*
 *
 */
void Logger::flush() {
    logging::core::get()->flush();
}

/**
 *
 */
void Logger::log(const SeverityLevel level, const string& message, const Value* pJsonData) const {
    sources::severity_logger_mt<SeverityLevel> sevLogger;

    if (pJsonData) {
        Value jsonCombinedMessage;

        jsonCombinedMessage[JSON_KEY_MSG] = message;
        jsonCombinedMessage[JSON_KEY_DATA] = *pJsonData;

        BOOST_LOG_SEV(sevLogger, level) << (JSON_DATA_MSG_START + writeString(*m_streamWriterBuilder, jsonCombinedMessage) + JSON_DATA_MSG_END);
    } else {
        BOOST_LOG_SEV(sevLogger, level) << message;
    }
}
