/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#include <atomic>
#include <chrono>
#include <map>
#include <memory>
#include <string>
#include <sys/wait.h>
#include <algorithm>
#include <iostream>
#include <string>
#include <memory>
#include <exception>

#include "logging.hpp"
#include "metrics.hpp"
#include "spellcheck_service.hpp"
#include "spellcheck_config.hpp"

using namespace std;

// -----------------------------------------------------------------------------

#define DEFAULT_SERVICE_NAME "spellcheck"
#define DEFAULT_INSTALL_PATH "/opt/open-xchange/" DEFAULT_SERVICE_NAME
#define DEFAULT_CONFIG_PATH DEFAULT_INSTALL_PATH "/etc/"
#define DEFAULT_CONFIG_FILE DEFAULT_CONFIG_PATH "spellcheck.properties"

typedef vector<string> StringVector;
typedef map<string, StringVector> ArgumentMap;

static atomic_int httpServicePid(0);
static bool signalHandlersInstalled = false;

/*
 * Implementation
 */
static void implHelp() {
    cout <<
    "Usage: spellcheck [-c ${CONFIGURATION_FILE}] [-d] [-h]" << endl << endl <<
    "-c ${CONFIGURATION_FILE} [Optional, Default: '" << DEFAULT_CONFIG_FILE << "']" << endl <<
    "-d enable debug switch, using single process only [Optional, Default: n/a]" << endl <<
    "-h this help page [Optional, Default: n/a]" << endl <<
    endl;
}

// -----------------------------------------------------------------------------

static void signalHandler(int signalNumber) {
    // kill child process as well if existent, if main process got signaled
    const int currentChildPid = httpServicePid.exchange(0);

    if (currentChildPid) {
        SPC_LOG->info("SpellCheck received interrupt signal: " + to_string(signalNumber) + " => Terminating WebService child process: " + to_string(currentChildPid));
        kill(currentChildPid, signalNumber);
    }

    exit(signalNumber);
}

// -----------------------------------------------------------------------------

static void implCreateArgvMap(ArgumentMap& arguments, int argc, char* argv[]) {

    string lastKey;

    for (int i = 1; i < argc; i++) {
        const string curArg(argv[i]);

        if (!curArg.empty()) {
            if (curArg.front() == '-') {
                lastKey = curArg;
                arguments.insert(make_pair(curArg, StringVector()));
            } else {
                arguments.find(lastKey)->second.push_back(curArg);
            }
        }
    }
}

/*
 * returns NULL if key is not available
 */
static const StringVector* implGetArgValues(const ArgumentMap& argvMap, string key) {
    ArgumentMap::const_iterator iter = argvMap.find(key);

    return (iter != argvMap.end()) ? &(iter->second) : NULL;
}

// -----------------------------------------------------------------------------

static const string implGetArgValue(const ArgumentMap& argvMap, string key, string defaultValue) {
    const StringVector* values = implGetArgValues(argvMap, key);

    return values ? values->front() : defaultValue;

}

// -----------------------------------------------------------------------------

static bool implHasKey(const ArgumentMap& argvMap, string key) {
    return implGetArgValues(argvMap, key) != NULL;
}

// -----------------------------------------------------------------------------

static shared_ptr<Logger> implCreateLogger(const shared_ptr<SpellCheckConfig> cfg) {
    return Logger::create(Logger::Config(cfg->getLogSeverity(), cfg->getLogPath(), cfg->isLogConsole()));
}

// -----------------------------------------------------------------------------

static int startHttpService(const shared_ptr<SpellCheckConfig> cfg) {
    // initialize DictManager once => the DictManager object, initialized this way, will be available in child processes as well;
    shared_ptr<DictManager> dictManager;

    {
        // the global Logger object needs to be initialized within the parent and child processes again after forking to avoid sharing of
        // one Logger object between parent and child processes which is problematic due to file desciptor handling for each process
        const shared_ptr<Logger> logger = implCreateLogger(cfg);

        dictManager = make_shared<DictManager>(cfg->getDictionaryPath(), cfg->getDictionaryTimeoutSeconds() * 1000UL, cfg->getSuggestionsMaxCacheSize());
        logger->flush();
    }

    int serverStartCount = 0;

    while (++serverStartCount) {
        const pid_t forkPid = (cfg->isDebug() ? 0 : fork());

        if (forkPid > 0) {
            // create own Logger for parent process
            const shared_ptr<Logger> logger = implCreateLogger(cfg);

            // globally store Pid of child process to be able to destroy
            // it in case the parent process got signaled
            httpServicePid.store(forkPid);

            // install signal handlers for parent process only once
            if (!signalHandlersInstalled) {
                static int signalsToWatch[] = {
                    SIGABRT,
                    SIGINT,
                    SIGSEGV,
                    SIGTERM
                };

                for (const auto& i : signalsToWatch) {
                    signal(i, signalHandler);
                }

                signalHandlersInstalled = true;
            }

            int serviceStatus = 0;

            SPC_LOG->info(string("SpellCheck started WebService with PID: ") + to_string(forkPid));
            SPC_LOG->flush();

            waitpid(forkPid, &serviceStatus, WUNTRACED);
            httpServicePid.store(0);

            string serviceExitMessage;
            string serviceExitText;
            int serviceExitCode;

            if (WIFSIGNALED(serviceStatus)) {
                serviceExitMessage = "got signaled";
                serviceExitCode = WTERMSIG(serviceStatus);
                serviceExitText = WCOREDUMP(serviceStatus);
            } else if (WIFSTOPPED(serviceStatus)) {
                serviceExitMessage = "was stopped";
                serviceExitCode = WSTOPSIG(serviceStatus);
            } else if (WIFEXITED(serviceStatus)) {
                serviceExitMessage = "exited";
                serviceExitCode = WEXITSTATUS(serviceStatus);
            } else {
                serviceExitMessage = "ended due to unkown event";
            }

            SPC_LOG->warn(string("SpellCheck WebService ") + serviceExitMessage +
                          ". Code/Signal: " + to_string(serviceExitCode) +
                          ". Message: " + ((serviceExitText.length() > 0) ? serviceExitText : "n/a"));
            SPC_LOG->flush();
        } else if (forkPid < 0) {
            throw exception();
        } else {
            // Spawned child process with own logger
            const shared_ptr<Logger> logger = implCreateLogger(cfg);
            string metricsBindAddress = cfg->getListenAddressMetrics();
            const string metricsAddressToUse = metricsBindAddress.append(":").append(to_string(cfg->getPortMetrics()));
            const shared_ptr<Metrics> metrics = Metrics::create(metricsAddressToUse, serverStartCount, cfg, dictManager);
            vector<SpellCheckSharedPtr> spellCheckVector;

            for (int i = 0; i < cfg->getThreadCount(); ++i) {
                spellCheckVector.push_back(make_shared<SpellCheck>(dictManager));
            }

            // start dictonary timeout handling
            dictManager->startDictionaryTimeoutHandling();

            if (cfg->isSSLEnabled()) {
                if (cfg->getSSLKeyFile().length() < 1) {
                    SPC_LOG->error("SpellCheck SSL support is enabled but no SSL key file is configured, disabling HTTPS transport!");
                }

                if (cfg->getSSLCertFile().length() < 1) {
                    SPC_LOG->error("SpellCheck SSL support is enabled but no SSL certificate file is configured, disabling HTTPS transport!");
                }
            }

            SpellCheckService spellCheckService(spellCheckVector, cfg);
            const bool sslEnabled = cfg->isSSLEnabled();
            unique_ptr<HttpServer> httpServer = make_unique<HttpServer>(
                cfg->getListenAddress(),
                cfg->getPort(),
                cfg->getRequestThreadCount(),
                cfg->getRequestTimeout(),
                spellCheckService,
                sslEnabled ? cfg->getSSLCertFile() : CONFIG_STRING_EMPTY,
                sslEnabled ? cfg->getSSLKeyFile() : CONFIG_STRING_EMPTY,
                sslEnabled ? cfg->getSSLKeyPassword() : CONFIG_STRING_EMPTY);

            SPC_LOG->info(string("SpellCheck WebService started, listening at ") + cfg->getListenAddress() + ':' + to_string(cfg->getPort()));
            SPC_LOG->info(std::string("SpellCheck metrics WebService started, listening at ") + cfg->getListenAddressMetrics() + ':' + to_string(cfg->getPortMetrics()));
            SPC_LOG->flush();

            httpServer->run();

            SPC_LOG->flush();
        }
    }

    return 0;
}

/*
 * Main function
 */
int main(int argc, char* argv[]) {
    int exitValue = 0;

    try {
        /* collecting program arguments... */
        ArgumentMap argvMap;

        implCreateArgvMap(argvMap, argc, argv);

        if (implHasKey(argvMap, "-h")) {
            implHelp();
        } else {
            const shared_ptr<SpellCheckConfig> cfg = make_shared<SpellCheckConfig>(implGetArgValue(argvMap, "-c", DEFAULT_CONFIG_PATH));

            cfg->setDebug(implGetArgValues(argvMap, "-d") != nullptr);

            try {
                exitValue = startHttpService(cfg);
            } catch (exception& e) {
                const shared_ptr<Logger> logger = Logger::create(Logger::Config(cfg->getLogSeverity(), cfg->getLogPath(), cfg->isLogConsole()));
                SPC_LOG->error(string("SpellCheck WebService could not be started: ") + e.what());
                exitValue = 2;
            }
        }
    } catch (exception& excp) {
        exitValue = 1;
    }

    return exitValue;
}
