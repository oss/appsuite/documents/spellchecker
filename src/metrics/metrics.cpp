/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

#include "metrics.hpp"

using namespace std;

static const double SERVER_VERSION = 1.0;

static const vector<string> REQUEST_NAMES = { "health", "check-spelling", "check-paragraph-spelling", "suggest-replacements", "is-misspelled", "supported-locales" };
static const vector<double> REQUEST_TIME_BUCKETS = { 0.001, 0.002, 0.005, 0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0 };

static const string TAG_ACCESSED("accessed");
static const string TAG_CACHE("cache");
static const string TAG_CURRENTCOUNT("currentCount");
static const string TAG_HIT("hit");
static const string TAG_LOADED("loaded");
static const string TAG_LOADERROR("loadError");
static const string TAG_LOCALE("locale");
static const string TAG_MAXCOUNT("maxCount");
static const string TAG_METHOD("method");
static const string TAG_PROPERTY("property");
static const string TAG_RESET("reset");
static const string TAG_SCOPE("scope");
static const string TAG_STATUS("status");
static const string TAG_STATUSCODE("statusCode");
static const string TAG_SUGGESTIONS("suggestions");
static const string TAG_TOTAL("total");
static const string TAG_TYPE("type");
static const string TAG_UNLOADED("unloaded");

/*
 * Metrics
 */

std::mutex Metrics::m_mutex;
std::shared_ptr<Metrics> Metrics::m_singleton;

// -------------------------------------------------------------------------

std::shared_ptr<Metrics> Metrics::create(const std::string& listenAddress, const int serverStarts, const std::shared_ptr<SpellCheckConfig> config, const std::shared_ptr<DictManager> dictManager) {
    unique_lock<mutex> m_singletonMutexLock(m_mutex);

    if (!m_singleton) {
        m_singleton = std::make_shared<Metrics>(listenAddress, serverStarts, config, dictManager);
    }

    return m_singleton;
}

// -------------------------------------------------------------------------

std::shared_ptr<Metrics> Metrics::get() {
    unique_lock<mutex> m_singletonMutexLock(m_mutex);
    return m_singleton;
}

// -------------------------------------------------------------------------

Metrics::Metrics(const std::string& listenAddress, const int serverStarts, const std::shared_ptr<SpellCheckConfig> config, const std::shared_ptr<DictManager> dictManager) :
    m_prometheusExposer(listenAddress),
    m_prometheusRegistry(std::make_shared<prometheus::Registry>()),
    m_serverStatusFamily(prometheus::BuildGauge().Name("ServerStatus").Help("The server properties").Labels(std::map<std::string, std::string>()).Register(*m_prometheusRegistry)),
    m_requestCountFamily(prometheus::BuildCounter().Name("RequestCount").Help("The number of processed 'oxspell' Http(s) requests").Labels(std::map<std::string, std::string>()).Register(*m_prometheusRegistry)),
    m_requestTimeFamily(prometheus::BuildHistogram().Name("RequestTime").Help("The request processing time for 'oxspell' Http(s) requests").Labels(std::map<std::string, std::string>()).Register(*m_prometheusRegistry)),
    m_dictionaryCountFamily(prometheus::BuildCounter().Name("DictionaryCount").Help("The number of loaded/unloaded dictionaries for each supported locale").Labels(std::map<std::string, std::string>()).Register(*m_prometheusRegistry)),
    m_cacheCountFamily(prometheus::BuildCounter().Name("Cache").Help("The cache access for 'oxspell' Http(s) requests").Labels(std::map<std::string, std::string>()).Register(*m_prometheusRegistry)),
    m_cacheStatusFamily(prometheus::BuildGauge().Name("CacheStatus").Help("The cache statistics for 'oxspell' Http(s) requests").Labels(std::map<std::string, std::string>()).Register(*m_prometheusRegistry)) {

    // ServerStatus
    const GaugePtr serverVersionProperty = &m_serverStatusFamily.Add({ { TAG_PROPERTY, "serverVersion" } });
    const GaugePtr serverStartsProperty = &m_serverStatusFamily.Add({ { TAG_PROPERTY, "serverStarts" } });
    const GaugePtr threadCountProperty = &m_serverStatusFamily.Add({ { TAG_PROPERTY, "threadCount" } });
    const GaugePtr requestThreadCountProperty = &m_serverStatusFamily.Add({ { TAG_PROPERTY, "requestThreadCount" } });
    const GaugePtr dictionaryTimeoutProperty = &m_serverStatusFamily.Add({ { TAG_PROPERTY, "dictionaryTimeout" } });
    const GaugePtr requestTimeoutProperty = &m_serverStatusFamily.Add({ { TAG_PROPERTY, "requestTimeout" } });
    const GaugePtr suggestionsMaxCacheSizeProperty = &m_serverStatusFamily.Add({ { TAG_PROPERTY, "suggestionsMaxCacheSize" } });

    // Total RequestCount
    m_requestCountTotal = &m_requestCountFamily.Add({ { "httpRequestCount_total", TAG_TOTAL } });

    // init Counter and Histogram maps for all requestNames
    for (const auto& curRequestName : REQUEST_NAMES) {
        m_requestCountsCode[curRequestName][200] = &m_requestCountFamily.Add({ { TAG_METHOD, curRequestName }, { TAG_STATUSCODE, "200" } });
        m_requestCountsCode[curRequestName][400] = &m_requestCountFamily.Add({ { TAG_METHOD, curRequestName }, { TAG_STATUSCODE, "400" } });
        m_requestCountsCode[curRequestName][408] = &m_requestCountFamily.Add({ { TAG_METHOD, curRequestName }, { TAG_STATUSCODE, "408" } });
        m_requestCountsCode[curRequestName][429] = &m_requestCountFamily.Add({ { TAG_METHOD, curRequestName }, { TAG_STATUSCODE, "429" } });
        m_requestCountsCode[curRequestName][500] = &m_requestCountFamily.Add({ { TAG_METHOD, curRequestName }, { TAG_STATUSCODE, "500" } });

        m_requestCountsError[curRequestName] = &m_requestCountFamily.Add({ { TAG_METHOD, curRequestName }, { TAG_STATUSCODE, "error" } });

        m_requestTimes[curRequestName] = &m_requestTimeFamily.Add({ { TAG_METHOD, curRequestName } }, REQUEST_TIME_BUCKETS);
    }

    // DictManager and locale based SuggestionCaches
    if (dictManager) {
        for (const auto& curLocaleAndEncoding : dictManager->getLocalesAndEncodings()) {
            const auto& curLocale = curLocaleAndEncoding.first;

            m_dictionary[DictionaryStatus::UNLOADED][curLocale] = &m_dictionaryCountFamily.Add({ { TAG_STATUS, TAG_UNLOADED }, { TAG_LOCALE, curLocale } });
            m_dictionary[DictionaryStatus::LOADED][curLocale] = &m_dictionaryCountFamily.Add({ { TAG_STATUS, TAG_LOADED }, { TAG_LOCALE, curLocale } });
            m_dictionary[DictionaryStatus::LOAD_ERROR][curLocale] = &m_dictionaryCountFamily.Add({ { TAG_STATUS, TAG_LOADERROR }, { TAG_LOCALE, curLocale } });
            m_dictionary[DictionaryStatus::ACCESSED][curLocale] = &m_dictionaryCountFamily.Add({ { TAG_STATUS, TAG_ACCESSED }, { TAG_LOCALE, curLocale } });

            // Cache Counts
            m_suggestionsCache[CacheCount::ACCESS][curLocale] = &m_cacheCountFamily.Add({ { TAG_TYPE, TAG_SUGGESTIONS }, { TAG_SCOPE, TAG_ACCESSED }, { TAG_LOCALE, curLocale } });
            m_suggestionsCache[CacheCount::HIT][curLocale] = &m_cacheCountFamily.Add({ { TAG_TYPE, TAG_SUGGESTIONS }, { TAG_SCOPE, TAG_HIT }, { TAG_LOCALE, curLocale } });
            m_suggestionsCache[CacheCount::RESET][curLocale] = &m_cacheCountFamily.Add({ { TAG_TYPE, TAG_SUGGESTIONS }, { TAG_SCOPE, TAG_RESET }, { TAG_LOCALE, curLocale } });

            // Cache Properties
            m_suggestionsCacheStatus[CacheStatus::MAX][curLocale] = &m_cacheStatusFamily.Add({ { TAG_TYPE, TAG_SUGGESTIONS }, { TAG_PROPERTY, TAG_MAXCOUNT }, { TAG_LOCALE, curLocale } });
            m_suggestionsCacheStatus[CacheStatus::CURRENT][curLocale] = &m_cacheStatusFamily.Add({ { TAG_TYPE, TAG_SUGGESTIONS }, { TAG_PROPERTY, TAG_CURRENTCOUNT }, { TAG_LOCALE, curLocale } });
        }

        // Cache Counts
        m_suggestionsCache[CacheCount::ACCESS][LOCALES_TOTAL] = &m_cacheCountFamily.Add({ { TAG_TYPE, TAG_SUGGESTIONS }, { TAG_SCOPE, TAG_ACCESSED }, { TAG_LOCALE, LOCALES_TOTAL } });
        m_suggestionsCache[CacheCount::HIT][LOCALES_TOTAL] = &m_cacheCountFamily.Add({ { TAG_TYPE, TAG_SUGGESTIONS }, { TAG_SCOPE, TAG_HIT }, { TAG_LOCALE, LOCALES_TOTAL } });
        m_suggestionsCache[CacheCount::RESET][LOCALES_TOTAL] = &m_cacheCountFamily.Add({ { TAG_TYPE, TAG_SUGGESTIONS }, { TAG_SCOPE, TAG_RESET }, { TAG_LOCALE, LOCALES_TOTAL } });
    }

    // ask the exposer to scrape the registry on incoming scrapes
    m_prometheusExposer.RegisterCollectable(m_prometheusRegistry);

    const ulong suggestionsMaxCacheSize = config->getSuggestionsMaxCacheSize();

    // Server status properties
    serverVersionProperty->Set(SERVER_VERSION);
    serverStartsProperty->Set(serverStarts);
    threadCountProperty->Set(config->getThreadCount());
    requestThreadCountProperty->Set(config->getRequestThreadCount());
    dictionaryTimeoutProperty->Set(config->getDictionaryTimeoutSeconds());
    requestTimeoutProperty->Set(config->getRequestTimeout() * 0.001);
    suggestionsMaxCacheSizeProperty->Set(suggestionsMaxCacheSize);

    // initialize suggestionsCacheMaxCountProperty for each locale
    if (dictManager)  {
        for (const auto& curLocaleAndEncoding : dictManager->getLocalesAndEncodings()) {
            const auto& curLocale = curLocaleAndEncoding.first;

            m_suggestionsCacheStatus[CacheStatus::MAX][curLocale]->Set(suggestionsMaxCacheSize);
            m_suggestionsCacheStatus[CacheStatus::CURRENT][curLocale]->Set(0);
        }
    }
}
