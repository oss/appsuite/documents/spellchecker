DOCKER_ID=$(docker run -d -v spellcheck.properties:/opt/open-xchange/spellcheck/spellcheck.properties -p 8002:8002 -p 8003:8003  registry.gitlab.open-xchange.com/documents/spellchecker:main)
sleep 10

./gradlew --info integrationTest

docker container stop $DOCKER_ID