import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import io.restassured.response.Response;
import io.restassured.response.ResponseBodyData;
import io.restassured.RestAssured;
import java.lang.System;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SpellCheckTest {

    final private String KEY_LENGTH = "length";
    final private String KEY_LOCALE = "locale";
    final private String KEY_NO_REPLACEMENTS = "noReplacements";
    final private String KEY_PARAGRAPH = "paragraph";
    final private String KEY_REPLACEMENTS = "replacements";
    final private String KEY_SPELL_RESULT = "spellResult";
    final private String KEY_START = "start";
    final private String KEY_TEXT = "text";
    final private String KEY_WORD = "word";
    final private String LOCALE_EN_US = "en_US";
    final private String MIME_TYPE_JSON = "application/json";

    // -------------------------------------------------------------------------

    @BeforeAll
    public static void setUp() {
        RestAssured.baseURI = "http://127.0.0.1";
        RestAssured.port = 8003;
    }

    // -------------------------------------------------------------------------

    @Test
    @DisplayName("Check for /live reponse code 200")
    public void test_01_Lifeness() {
        when().
            get("/live").
        then().
            assertThat().statusCode(200);
    }

    // -------------------------------------------------------------------------

    @Test
    @DisplayName("Check for /ready reponse code 200")
    public void test_02_Readyness() {
        when().
            get("/live").
        then().
            assertThat().statusCode(200);
    }

    // -------------------------------------------------------------------------

    @Test
    @DisplayName("Check for /health reponse code 200 and correct serverId of SpellCheck service in response")
    public void test_03_Health() {
        when().
            get("/health").
        then().
            assertThat().statusCode(200).and().
            body("health.serverId", equalTo("77fdc062-570b-11ea-91c3-8bd6e5445cb5")).
            log().all(true);
    }

    // -------------------------------------------------------------------------

    @Test
    @DisplayName("Call /supported-locales and check for at least one supported locale")
	public void test_04_SupportedLocales() throws Exception {
        final Response response =
            when().
                get("/supported-locales").
            then().
                assertThat().statusCode(200).and().
                log().all(true).
                contentType(MIME_TYPE_JSON).
                extract().
                response();

        try {
            JSONObject jsonResponse = new JSONObject(response.getBody().asString());

            assertNotNull(jsonResponse);

            assertTrue(jsonResponse.has("supportedLocales") && (jsonResponse.optJSONArray("supportedLocales") != null));
            assertTrue(jsonResponse.getJSONArray("supportedLocales").length() > 0);
        } catch (Exception e) {
            fail(e.getLocalizedMessage());
        }
    }

    // -------------------------------------------------------------------------

    @Test
    @DisplayName("Call /check-paragraph-spelling with valid paragraph")
	public void test_05_ParagraphSpelling() throws Exception {
        final String goodParagraphText = "The quick brown fox jumps over the lazy dog.";
        final JSONObject goodResult = implCheckParagraphSpelling(goodParagraphText, LOCALE_EN_US, true);

        assertNotNull(goodResult);

        final JSONArray goodResultArray = goodResult.getJSONArray(KEY_SPELL_RESULT);

        assertTrue(goodResultArray.length() == 0);
    }

    // -------------------------------------------------------------------------

    @Test
    @DisplayName("Call /check-paragraph-spelling with misspelled paragraph and getting replacements in response")
	public void test_06_MisspelledParagraphSpellingWithReplacements() throws Exception {
        // test misspelled paragraphs with replacements
        implCheckMisspelledParagraph(false);
    }

    // -------------------------------------------------------------------------

    @Test
    @DisplayName("Call /check-paragraph-spelling with misspelled paragraph and suppressing replacements in response")
	public void test_07_MisspelledParagraphSpellingWithoutReplacements() throws Exception {
        // test misspelled paragraphs without replacements
        implCheckMisspelledParagraph(true);
    }

    // -------------------------------------------------------------------------

	@Test
    @DisplayName("Call /check-spelling with valid word")
	public void test_08_WordSpelling() throws Exception {
        // spellcheck a correct/known word
        final String goodWord = "brown";
        final JSONObject goodResult = implCheckWordSpelling(goodWord, LOCALE_EN_US, false);
        final JSONArray goodResultArray = goodResult.getJSONArray(KEY_SPELL_RESULT);

        assertNotNull(goodResultArray);
        assertTrue(goodResultArray.length() == 0);
	}

    // -------------------------------------------------------------------------

    @Test
    @DisplayName("Call /check-spelling with misspelled word")
	public void test_09_MisspelledWordSpellingWithReplacements() throws Exception {
        // spellcheck an incorrect/unknown word
        final String wrongWord = "junps";
        final JSONObject wrongResult = implCheckWordSpelling(wrongWord, LOCALE_EN_US, false);
        final JSONArray wrongResultArray = wrongResult.getJSONArray(KEY_SPELL_RESULT);

        assertTrue(wrongResultArray.length() == 1);

        // check correction for error "junps"
        final JSONObject wrongResultObject = wrongResultArray.getJSONObject(0);

        assertNotNull(wrongResultObject);
        assertEquals(wrongResultObject.getInt(KEY_START), 0);
        assertEquals(wrongResultObject.getInt(KEY_LENGTH), wrongWord.length());
        assertEquals(wrongResultObject.getString(KEY_WORD), wrongWord);
        assertNotNull(wrongResultObject.getJSONArray(KEY_REPLACEMENTS));
        assertTrue(wrongResultObject.getJSONArray(KEY_REPLACEMENTS).length() > 0);
	}

    // -------------------------------------------------------------------------

    private JSONObject implCheckParagraphSpelling(final String paragraphText, final String locale, boolean noReplacements) throws Exception {
		final JSONArray jsonParagraphs = new JSONArray().put(new JSONObject().put(KEY_WORD, paragraphText).put(KEY_LOCALE, locale));
        final JSONObject jsonRequestBody = new JSONObject().put(KEY_PARAGRAPH, jsonParagraphs).put(KEY_NO_REPLACEMENTS, noReplacements);
        final Response response =
            given().
                contentType(MIME_TYPE_JSON).
                accept(MIME_TYPE_JSON).
                body(jsonRequestBody.toString()).
            when().
                post("/check-paragraph-spelling").
            then().
                assertThat().statusCode(200).and().
                log().all(true).
                contentType("application/json").
                extract().
                response();

        assertNotNull(response);

        final ResponseBodyData body = response.getBody();

        assertNotNull(body);

        return new JSONObject(body.asString());
	}

    // -------------------------------------------------------------------------

    private void implCheckMisspelledParagraph(boolean noReplacements) throws Exception {
        // spellcheck an incorrect paragraph text and optionally get replacements
        final String wrongWord1 = "queck";
        final String wrongWord2 = "junps";
        final String wrongWord3 = "laty";
        final String wrongParagraphText = "The " + wrongWord1 + " brown fox " + wrongWord2 + " over the " + wrongWord3 + " dog.";

        final JSONObject misspelledResult = implCheckParagraphSpelling(wrongParagraphText, LOCALE_EN_US, noReplacements);

        assertNotNull(misspelledResult);

        // [{"start":4,"length":5,"replacements":["quick","quack"],"locale":"en_US","word":"queck"},
        //  {"start":20,"length":5,"replacements":["jumps","junks"],"locale":"en_US","word":"junps"},
        //  {"start":35,"length":4,"replacements":["lat","lay","laity","platy","late","lats","lacy","lady","lath","lazy","Katy","laty"],"locale":"en_US","word":"laty"}]
        final JSONArray misspelledResultArray = misspelledResult.getJSONArray(KEY_SPELL_RESULT);

        assertEquals(misspelledResultArray.length(), 3);

        // check correction for error "queck"
        final JSONObject correction1 = misspelledResultArray.getJSONObject(0);

        assertEquals(correction1.getInt(KEY_START), 4);
        assertEquals(correction1.getInt(KEY_LENGTH), wrongWord1.length());
        assertEquals(correction1.getString(KEY_WORD), wrongWord1);

        // check replacement length
        final int replacementLength1 = correction1.getJSONArray(KEY_REPLACEMENTS).length();
        final boolean replacementsOk1 = noReplacements ? (replacementLength1 == 0) : (replacementLength1 > 0);
        assertTrue(replacementsOk1);

        // check correction for error "junps"
        final JSONObject correction2 = misspelledResultArray.getJSONObject(1);

        assertEquals(correction2.getInt(KEY_START), 20);
        assertEquals(correction2.getInt(KEY_LENGTH), wrongWord2.length());
        assertEquals(correction2.getString(KEY_WORD), wrongWord2);

        // check replacement length
        final int replacementLength2 = correction2.getJSONArray(KEY_REPLACEMENTS).length();
        final boolean replacementsOk2 = noReplacements ? (replacementLength2 == 0) : (replacementLength2 > 0);
        assertTrue(replacementsOk2);

        // check correction for error "laty"
        final JSONObject correction3 = misspelledResultArray.getJSONObject(2);

        assertEquals(correction3.getInt(KEY_START), 35);
        assertEquals(correction3.getInt(KEY_LENGTH), wrongWord3.length());
        assertEquals(correction3.getString(KEY_WORD), wrongWord3);

        // check replacement length
        final int replacementLength3 = correction3.getJSONArray(KEY_REPLACEMENTS).length();
        final boolean replacementsOk3 = noReplacements ? (replacementLength3 == 0) : (replacementLength3 > 0);
        assertTrue(replacementsOk3);
    }

    // -------------------------------------------------------------------------

    private JSONObject implCheckWordSpelling(final String word, final String locale, boolean noReplacements) throws Exception {
		final JSONObject jsonRequestBody = new JSONObject().put(KEY_TEXT, word).put(KEY_LOCALE, locale).put(KEY_NO_REPLACEMENTS, noReplacements);
        final Response response =
            given().
                contentType(MIME_TYPE_JSON).
                accept(MIME_TYPE_JSON).
                body(jsonRequestBody.toString()).
            when().
                post("/check-spelling").
            then().
                assertThat().statusCode(200).and().
                log().all(true).
                contentType("application/json").
                extract().
                response();

        assertNotNull(response);

        final ResponseBodyData body = response.getBody();

        assertNotNull(body);

        return new JSONObject(body.asString());
	}
}