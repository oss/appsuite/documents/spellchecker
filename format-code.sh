#!/bin/bash

UNCRUSTIFY_FORMATTER=/usr/bin/uncrustify
UNCRUSTIFY_CONFIG=./uncrustify/uncrustify.cfg

if [[ -x "${UNCRUSTIFY_FORMATTER}" ]]; then
    "${UNCRUSTIFY_FORMATTER}" -c "${UNCRUSTIFY_CONFIG}" --no-backup $(find ./src \( -name "*.?pp" -or -name "*.?xx" -or -name "*.c" -or -name "*.h" \))
else
    echo "WARN: Could not find 'uncrustify' executable to format code before commit. Executing commit without code formatting, but please install 'uncrustify' package on system before next commit."
fi

exit 0
