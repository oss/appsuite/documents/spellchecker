#!/bin/bash

set -e

usage() {
    echo "Usage: build.sh [--help] | [--debug] [--clean] baseline|oxspell|all"
}

if [[ ${#} -lt 1 ]] || [[ ${1} =~ help ]]; then
    usage
    exit 0
fi

PRJ_DIR=$(pwd)
export DESTDIR=${PRJ_DIR}/build

# Set Debug build type, if requested (Default:Release)
BUILD_TYPE=Release

# check for --debug flag
if ( [[ ${#} -gt 0 ]] && [[ ${1} == "--debug" ]] ) || ( [[ ${#} -gt 1 ]] && [[ ${2} == "--debug" ]] ); then
    BUILD_TYPE=Debug
fi

# check for --debug flag
if ( [[ ${#} -gt 0 ]] && [[ ${1} == "--clean" ]] ) || ( [[ ${#} -gt 1 ]] && [[ ${2} == "--clean" ]] ); then
    rm -rf "${DESTDIR}"
fi

# iterate over all given targets sequentially
while [[ ${#} -gt 0 ]]; do
    TARGET=${1}
    shift

    # Build baseline libs if (target==all || target==baseline)
    if [[ ${TARGET} == all ]] || [[ ${TARGET} == baseline ]]; then
        echo "Building and installing baseline libraries to be statically linked by SpellCheck service executable"
        BASELINE_BUILD_DIR=${DESTDIR}/baseline_build
        BASELINE_SRC_DIR=${PRJ_DIR}/thirdparty

        if [[ ! -f ${BASELINE_BUILD_DIR} ]]; then
            mkdir -p "${BASELINE_BUILD_DIR}"
        fi

        pushd  "${BASELINE_BUILD_DIR}"
        cmake -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ${BASELINE_SRC_DIR}

        if make -j4; then
            echo "Build script successfully installed baseline libraries at following location: ${DESTDIR}/usr/local"
        else
            echo "Build script could not built baseline libraries at following location: ${DESTDIR}/usr/local"
        fi
        popd
    fi

    # Build and install 'oxspell' executable if (target==all || target==oxspell)
    if [[ ${TARGET} == all ]] || [[ ${TARGET} == oxspell ]]; then
        echo "Building SpellCheck service executable"
        OXSPELL_BUILD_DIR=${DESTDIR}/oxspell_build
        OXSPELL_SRC_DIR=${PRJ_DIR}/src

        if [[ ! -f ${OXSPELL_BUILD_DIR} ]]; then
            mkdir -p "${OXSPELL_BUILD_DIR}"
        fi

        pushd  "${OXSPELL_BUILD_DIR}"
        cmake -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ${OXSPELL_SRC_DIR}
        if make -j4; then
            cp ${OXSPELL_BUILD_DIR}/oxspell ${DESTDIR}/usr/local/bin/

            if [[ ${BUILD_TYPE} == Release ]]; then
                objcopy --strip-unneeded ${DESTDIR}/usr/local/bin/oxspell
                echo "Build script stripped debug info from release executable"
            fi

            echo "Build script successfully installed executable at following location: ${DESTDIR}/usr/local/bin/oxspell"
        else
            echo "Build script could not built executable at following location: ${DESTDIR}/usr/local/bin/oxspell"
        fi
        popd
    fi
done
