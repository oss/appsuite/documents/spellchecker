#! /bin/bash

set -e

PATH_TO_NODE=/usr/bin/node

# combine swagger spec fragments into one swagger.json
# file to be used by subsequent tools
"${PATH_TO_NODE}" resolve.js ./spellcheck
