{
    "openapi": "3.0.0",
    "info": {
        "title": "SpellCheck service Http API",
        "description": "Documentation of the Open-Xchange GmbH SpellCheck service Http API.\n___",
        "contact": {
            "name": "Open-Xchange GmbH",
            "email": "info@open-xchange.com",
            "url": "https://www.gnu.org/licenses/gpl-2.0.htmlhttps://www.gnu.org/licenses/gpl-2.0.html"
        },
        "version": "7.10.4"
    },
    "servers": [
        {
            "url": "http://example.com:8003"
        }
    ],
    "tags": [
        {
            "name": "SpellCheck",
            "description": "The SpellCheck service module."
        }
    ],
    "components": {
        "schemas": {
            "request-check-paragraph-spelling": {
                "type": "object",
                "description": "Array of paragraphs to check spelling for.",
                "properties": {
                    "paragraph": {
                        "type": "array",
                        "description": "Paragraphs consisting of texts to check spelling for.",
                        "items": {
                            "type": "object",
                            "description": "Single text paragraph to check spelling for, using a given locale and required language encoding of the used dictionary.",
                            "properties": {
                                "locale": {
                                    "type": "string",
                                    "description": "The locale to be used to check the spelling of the given paragraph."
                                },
                                "word": {
                                    "type": "string",
                                    "description": "The paragraph to check spelling for, using the given locale and required language encoding of the used dictionary."
                                }
                            }
                        }
                    },
                    "noReplacements": {
                        "type": "boolean",
                        "description": "Optional property to specify if replacments should be retrieved and returned for the current request. Default is set to false, so that replacements are returned if not specified.",
                        "default": false
                    }
                }
            },
            "request-check-spelling": {
                "type": "object",
                "properties": {
                    "locale": {
                        "type": "string",
                        "description": "The locale to be used to check the spelling of the given text."
                    },
                    "text": {
                        "type": "string",
                        "description": "The text to check spelling for, using the given locale and required language encoding of the used dictionary."
                    },
                    "offset": {
                        "type": "integer",
                        "description": "Offset that is added to the appropriate character position within the response for detected, misspelled words."
                    },
                    "noReplacements": {
                        "type": "boolean",
                        "description": "Optional property to specify if replacments should be retrieved and returned for the current request. Default is set to false, so that replacements are returned if not specified.",
                        "default": false
                    }
                }
            },
            "request-locale-word": {
                "type": "object",
                "properties": {
                    "locale": {
                        "type": "string",
                        "description": "The locale to be used to check the spelling of the given word."
                    },
                    "word": {
                        "type": "string",
                        "description": "The word to check spelling for, using the given locale and required language encoding of the used dictionary."
                    }
                }
            },
            "response-check-spelling": {
                "type": "object",
                "description": "The result of the current spell operation.",
                "properties": {
                    "spellResult": {
                        "type": "array",
                        "description": "The array of misspelled words and properties within the given text.",
                        "items": {
                            "type": "object",
                            "description": "A currently misspelled word within the given text.",
                            "properties": {
                                "length": {
                                    "type": "integer",
                                    "description": "The length of the currently misspelled word within the given text."
                                },
                                "locale": {
                                    "type": "string",
                                    "description": "The locale that was used to check the currently misspelled word within the given text."
                                },
                                "start": {
                                    "type": "integer",
                                    "description": "The start position of the currently misspelled word within the given text, increased by the given offset within the request."
                                },
                                "word": {
                                    "type": "string",
                                    "description": "The currently misspelled word within the given text, encoded with the language encoding of the used dictionary."
                                },
                                "replacements": {
                                    "type": "array",
                                    "description": "The optional array of replacements for the currently misspelled word within the given text. Each replacement word is encoded with the language encoding of the used dictionary.",
                                    "nullable": true,
                                    "items": {
                                        "type": "string",
                                        "description": "The replacement word, encoded with the language encoding of the used dictionary for the requested locale."
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "response-health": {
                "type": "object",
                "description": "The health result for the currently running service.",
                "properties": {
                    "health": {
                        "type": "object",
                        "properties": {
                            "serverAPI": {
                                "type": "integer",
                                "description": "The numeric version of the currently supported API."
                            },
                            "serverAPIText": {
                                "type": "string",
                                "description": "The textual version of the currently supported API."
                            },
                            "serverId": {
                                "type": "string",
                                "description": "The unique id of the service."
                            },
                            "serverName": {
                                "type": "string",
                                "description": "The textual name of the service."
                            },
                            "serverStatus": {
                                "type": "string",
                                "description": "The current textual status of the service."
                            }
                        }
                    }
                }
            },
            "response-is-misspelled": {
                "type": "object",
                "description": "The result of the operation to check if a word is misspelled.",
                "properties": {
                    "spellResult": {
                        "type": "boolean",
                        "description": "true if the given word is misspelled, false otherwise"
                    }
                }
            },
            "response-suggest-replacements": {
                "type": "object",
                "description": "The result of the suggested replacements operation.",
                "properties": {
                    "spellResult": {
                        "type": "array",
                        "description": "The array of suggested replacements.",
                        "items": {
                            "type": "string",
                            "description": "The suggested replacement words for the misspelled input word. Each replacement word is encoded with the language encoding of the used dictionary."
                        }
                    }
                }
            },
            "response-supported-locales": {
                "type": "object",
                "description": "The result of the supported locales operation.",
                "properties": {
                    "spellResult": {
                        "type": "array",
                        "description": "The array of supported locales and encodings.",
                        "items": {
                            "type": "object",
                            "description": "The locale and encoding description for a supported dictionary. For each request and response, words and texts are encoded with the appropriate dictionary encoding.",
                            "properties": {
                                "locale": {
                                    "type": "string",
                                    "description": "The locale of the dictionary."
                                },
                                "encoding": {
                                    "type": "string",
                                    "description": "The language encoding to be used in spellcheck requests and responses for this locale. In case of non UTF-8 encodings, word and text characters need to be in the range 0x00-0xFF, independently from the used Http(s) transport encoding."
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    "paths": {
        "/check-paragraph-spelling": {
            "post": {
                "operationId": "checkParagraphSpelling",
                "tags": [
                    "SpellCheck"
                ],
                "summary": "Paragraph spelling request",
                "description": "Check spelling of the given paragraphs, using the given locales.",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/request-check-paragraph-spelling"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "The spelling result for the given paragraphs, using the given locales.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/response-check-spelling"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/check-spelling": {
            "post": {
                "operationId": "checkSpelling",
                "tags": [
                    "SpellCheck"
                ],
                "summary": "Text spelling request",
                "description": "Check spelling of the given text using the given locale.",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/request-check-spelling"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "The spelling result for the given text, using the given locale.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/response-check-spelling"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/health": {
            "get": {
                "operationId": "health",
                "tags": [
                    "SpellCheck"
                ],
                "summary": "Server Health Request",
                "description": "Getting the current health status of the SpellCheck service.",
                "responses": {
                    "200": {
                        "description": "The health status of the SpellCheck service.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/response-health"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/is-misspelled": {
            "post": {
                "operationId": "isMisspelled",
                "tags": [
                    "SpellCheck"
                ],
                "summary": "Misspelled request",
                "description": "Checking if a word is misspelled, using the given locale.",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/request-locale-word"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "The boolean result of the spell check operation, using the given locale.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/response-is-misspelled"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/suggest-replacements": {
            "post": {
                "operationId": "suggestReplacements",
                "tags": [
                    "SpellCheck"
                ],
                "summary": "Suggested replacements request",
                "description": "Retrieving the suggested replacements for a given word, using the given locale.",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/request-locale-word"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "The suggested replacements for a misspelled word, using the given locale.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/response-suggest-replacements"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/supported-locales": {
            "get": {
                "operationId": "supportedlocales",
                "tags": [
                    "SpellCheck"
                ],
                "summary": "Supported locales request",
                "description": "Retrieving all locales that are supported to be used for spell check requests.",
                "responses": {
                    "200": {
                        "description": "The supported locales for all spell check related requests.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/response-supported-locales"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}