Building
======== 
Use the provided 'build.sh' shell script to perform a local build of the 'oxspell' executable:

Usage: ./build.sh [--help] | [--debug] [--clean] baseline|oxspell|all

An appropriate C++ build toolchain needs to be available, supporting the CXX11 Standard as well
as some CXX14 extensions backported to the CXX11 compiler. Working toolchains tested are least:

  - gcc-6 (not guaranteed to work)
  - gcc-7
  - gcc-8
  - gcc-9
  - gcc-10
  - gcc-xx

The mininmal required cmake version on the system is:

  - cmake-3.13

The following executables need to be installed:

  - curl (for performing local test Http requests to locally built web service)
  - patch

The following library requirements are expected to be available on the system:

  - autoconf
  - automake
  - libcurl4-openssl-dev
  - libssl-dev
  - libtool

For e.g. installing the mentioned packages on a Debian based system, the following command should cover all packages:

  - sudo apt update && sudo apt install build-essential curl git patch cmake autoconf automake libcurl4-openssl-dev libssl-dev libtool

The project build itself consists of two subsequent build steps, that might be automatically performed.
The created output directory for all build steps is ${PRJ_DIR}/build

  1. Depending project baseline libraries are automatically downloaded, built and installed to ${PRJ_DIR}/build/usr/local.
  2. After the baseline prerequisites have been built and installed, the 'oxspell' executable is built and installed at ${PRJ_DIR}/build/usr/local/bin/oxspell.

Examples:
---------

Build baseline libraries and oxspell executable:

  ./build.sh --clean all

If baseline libraries have been built and are already available, the oxspell binary can be (re)built:

  ./build.sh oxspell

Use the --debug switch to enable Debug builds for oxspell executable:

  ./build.sh --debug oxspell

After building the 'oxspell' executable, the web service can be started within the shell via the following command.
A local configuration file that logs output to console only is used in this case:

  ./build/oxspell_build/oxspell -c ./spellcheck.properties

GUI based IDE development
=========================
Configuration files for MS VSCode based IDE development of the 'oxspell' executable are provided within the subdirectory ${PRJ_DIR}/.vscode.
If not contained within the default package repository of the system, the free VSCode IDE can be downloaded and installed from the following website: https://code.visualstudio.com

The VSCode IDE 'code' executable is expected to be installed at /usr/bin/code by the provided ${PRJ_DIR}/start-ide.sh script. If the 'code' executable is located at a different location,
please adjust the location within the ${PRJ_DIR}/start-ide.sh script:

  - VSCODE_PATH=/path/to/code

Before oxspell development within the IDE is possible, the appropriate prerequisites on the system need to be installed and the local baseline libraries need to be built first. Please
see section 'Building' above for all necessary requirements and build the local baseline libraries within a console:

  ./build.sh --clean baseline

After building the baseline libraries, the VSCode IDE can be started with the following command within the ${PRJ_DIR}:

  ./start-ide.sh

After the first startup of the IDE, the required extensions for the IDE are shown within a popup window as recommended extensions.
All recommended extensions need to be installed/accepted by the user to begin with 'oxspell' IDE development. Please restart the IDE after installing the extensions.
In detail, the following extensions are mandatory:

  - ms-vscode.cpptools
  - ms-vscode.cmake-tools

The correct toolchain for C++ development needs to be selected afterwards, which is asked for by the IDE after installing the extensions or building for the first time in general.
If not asked for by the IDE, choose the appropriate C++ development toolchain on your system by clicking the 'Change Active Kit' icon within the CMake status bar.

To build the 'oxspell' executable with debug information, switch to the CMake tab on the left side of the IDE. Afterwards click on the popup menu of the CMake tab (upper right corner of CMake tab itself) and choose:

  - Clean Reconfigure all projects
  - Clean Rebuild all projects

After building the executable, debugging is possible by passing the '-d' switch to the oxspell commandline, which prevents forking of 'oxspell' child processes within the debug session.
The provided default debug configuration already contains this switch.
Please ensure that no other 'oxspell' process is currently running on the system before starting a Run/Debug session within the IDE.

In order to use this default debug configuration, the debugger can either be started by pressing 'F5' or by choosing the following menu entry:

  - Run => Start debugging

The debug configuration itself can be viewed/edited by choosing the following menu entry:

  - Run => Open Configurations

Please don't use the Debug icon within the CMake status bar to start debugging since this will not use the provided default debug configuration that contains the '-d' switch to prevent process forking.


Packaging
=========
In order to create system repository packages (Debian and RPM based), the Gradle build framework is used.
To build packages locally within the ${PRJ_DIR}/build output directory:

  ./gradlew buildPackages


Continuous integration
======================
Continuous integration is performed via GitLab-CI and  configured by the file ./.gitlab-ci.yml within
the project root. Whenever new commits are pushed to the projects' Git remote repository on GitLab,
a new build, packaging and package publishing process is automaticaally triggered.
