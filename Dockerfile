################################################################################
# open-xchange-spellcheck build stage
################################################################################
FROM        registry-proxy.sre.cloud.oxoe.io/library/debian:bullseye-slim AS builder

WORKDIR     /build/open-xchange-spellcheck/

RUN         apt-get update && apt-get -y upgrade && apt-get install -y --no-install-recommends \
                apt-utils \
                autoconf \
                automake \
                build-essential \
                ca-certificates \
                cmake \
                git \
                libcurl4-openssl-dev \
                libgsasl-dev \
                libpsl-dev \
                libssl-dev \
                libtool \
                gnupg2 \
                patch \
                procps && \
            rm -rf /var/cache/apt/* /var/lib/apt/lists/*

COPY        ./ ./

RUN         ./build.sh --clean all

################################################################################
# open-xchange-spellcheck service stage
################################################################################
FROM        registry-proxy.sre.cloud.oxoe.io/library/debian:bullseye-slim

RUN         apt-get update && apt-get -y upgrade && apt-get install -y --no-install-recommends \
                ca-certificates \
                gnupg2 \
                hunspell \
                hunspell-en-us \
                hunspell-en-gb \
                hunspell-de-de \
                hunspell-fr \
                hunspell-fr-revised \
                hunspell-es \
                hunspell-el \
                hunspell-nl \
                hunspell-pl \
                hunspell-it \
                hunspell-hu \
                hunspell-sk \
                hunspell-cs \
                hunspell-lv \
                hunspell-ro \
                hunspell-pt-br \
                hunspell-da \
                hunspell-ru \
                hunspell-tr \
                procps && \
            rm -rf /var/cache/apt/* /var/lib/apt/lists/*

WORKDIR     /opt/open-xchange/spellcheck

RUN         mkdir -p /var/log/open-xchange/spellcheck ./bin ./etc

COPY        --from=builder /build/open-xchange-spellcheck/build/usr/local/bin/oxspell ./bin/
COPY        --from=builder /build/open-xchange-spellcheck/conf/spellcheck.properties ./etc/

ENV         TINI_VERSION v0.19.0
ADD         https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN         chmod +x /tini && \
            groupadd -r -g 1000 open-xchange && \
            useradd -r -g open-xchange -u 987 open-xchange

RUN         chown -R open-xchange:open-xchange /opt/open-xchange
RUN         mkdir -p /var/log/open-xchange && chown -R open-xchange:open-xchange /var/log/open-xchange && \
            mkdir -p /var/spool/open-xchange && chown -R open-xchange:open-xchange /var/spool/open-xchange && \
            mkdir -p /var/opt && chown -R open-xchange:open-xchange /var/opt

USER        open-xchange:open-xchange

ENTRYPOINT  [ "/tini", "-g", "--", "/bin/sh", "-c", \
              "/opt/open-xchange/spellcheck/bin/oxspell -c /opt/open-xchange/spellcheck/etc/spellcheck.properties" \
            ]

EXPOSE      8002 8003
