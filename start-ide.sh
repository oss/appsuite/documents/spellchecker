#! /bin/bash

VSCODE_PATH=/usr/bin/code

export DESTDIR=./build

if [[ -x ${VSCODE_PATH} ]]; then
    "${VSCODE_PATH}" ./
else 
    echo "VSCode cannot be found. Please install or adjust VSCODE_PATH environment variable within this script: ${VSCODE_PATH}"
fi

exit 0
